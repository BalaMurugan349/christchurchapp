//
//  Audio+CoreDataProperties.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/15/16.
//  Copyright © 2016 Bose. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Audio {

    @NSManaged var name: String?
    @NSManaged var createdDate: String?
    @NSManaged var duration: String?
    @NSManaged var fileSize: String?
    @NSManaged var isExpanded: NSNumber?

}
