//
//  Audio.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/15/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import Foundation
import CoreData


class Audio: NSManagedObject {

    class func saveAudioToCoreData (filename : String , createdDate : String, duration : String , fileSize : String){
        let entityDescription = NSEntityDescription.entityForName("Audio", inManagedObjectContext: AppDelegate.sharedInstance().managedObjectContext)
        let audioObj = NSManagedObject(entity: entityDescription!, insertIntoManagedObjectContext: AppDelegate.sharedInstance().managedObjectContext) as! Audio
        audioObj.name = filename
        audioObj.createdDate = createdDate
        audioObj.duration = duration
        audioObj.fileSize = fileSize
        audioObj.isExpanded = false
        do{
            try AppDelegate.sharedInstance().managedObjectContext.save()
        }catch{
            
        }
        
        
    }
    
    class func fetchAudioFiles () -> [Audio]{
        let fetchRequest = NSFetchRequest()
        let entityDescription = NSEntityDescription.entityForName("Audio", inManagedObjectContext:AppDelegate.sharedInstance().managedObjectContext)
        fetchRequest.entity = entityDescription
        
        do {
            let result = try AppDelegate.sharedInstance().managedObjectContext.executeFetchRequest(fetchRequest)
            return result as! [Audio]
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        return [Audio]()
    }

    class func deleteAudio (audioObj : Audio){
        let context:NSManagedObjectContext = AppDelegate.sharedInstance().managedObjectContext
        context.deleteObject(audioObj)
        do{
            try AppDelegate.sharedInstance().managedObjectContext.save()
        }catch{
            
        }

    }
}
