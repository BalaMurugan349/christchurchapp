//
//  Extension.swift
//  ChristChurchApp
//
//  Created by Bose on 29/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import Foundation
import UIKit
import KYDrawerController
import SDWebImage


extension UIViewController {

    var sideMenuController : KYDrawerController {
        get {
            return (UIApplication.sharedApplication().delegate as! AppDelegate).window?.rootViewController as! KYDrawerController
        }
    }
}


extension NSError {
    
    convenience init(errorMessage : String) {
        self.init(domain: "Error", code: 101, userInfo: [NSLocalizedDescriptionKey : errorMessage])
    }
    
}


extension UIImageView {
    func image(url: String) {
        self.sd_setImageWithURL(NSURL(string: url), placeholderImage: nil, options: .RefreshCached) { (image, error, cacheType, url) in
            
        }
    }
}


extension String{
    
    func getDateForMilliseconds(dateFormat : String) -> String{
    let intValue : Double =  NSString(string: self).doubleValue
    let date = NSDate(timeIntervalSince1970: intValue/1000.0)
    let dateFormatter : NSDateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = dateFormat
    return dateFormatter.stringFromDate(date)
    
    }
    
    //MARK:- CALCULATE HEIGHT FOR LABEL
    func heightForLabel (font:UIFont, width:CGFloat) -> CGSize{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = self
        label.sizeToFit()
        return label.frame.size
    }
    
    var isValidEmail : Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(self)
    }


}


extension NSDate {
    func yearsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Year, fromDate: date, toDate: self, options: []).year
    }
    func monthsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Month, fromDate: date, toDate: self, options: []).month
    }
    func weeksFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.WeekOfYear, fromDate: date, toDate: self, options: []).weekOfYear
    }
    func daysFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Day, fromDate: date, toDate: self, options: []).day
    }
    func hoursFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Hour, fromDate: date, toDate: self, options: []).hour
    }
    func minutesFrom(date: NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Minute, fromDate: date, toDate: self, options: []).minute
    }
    func secondsFrom(date: NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Second, fromDate: date, toDate: self, options: []).second
    }
    func offsetFrom(date: NSDate) -> String {
        if yearsFrom(date)   > 0 { return "\(yearsFrom(date)) years ago"   }
        if monthsFrom(date)  > 0 { return "\(monthsFrom(date)) months ago"  }
        if weeksFrom(date)   > 0 { return "\(weeksFrom(date)) weeks ago"   }
        if daysFrom(date)    > 0 { return "\(daysFrom(date)) days ago"    }
        if hoursFrom(date)   > 0 { return "\(hoursFrom(date)) hours ago"   }
        if minutesFrom(date) > 0 { return "\(minutesFrom(date)) mins ago" }
        if secondsFrom(date) > 0 { return "\(secondsFrom(date))secs ago" }
        return ""
    }
}

extension UITextField {
    var isEmpty : Bool {
        return self.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0
}
}


class Extention: NSObject {
    class func alert(message : String) -> UIAlertController {
        let alertController = UIAlertController(title: "Christ Center", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Destructive) { (action) -> Void in
            
        }
        alertController.addAction(alertOkAction)
        return alertController
    }
}
