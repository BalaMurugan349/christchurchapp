//
//  CCLabel.swift
//  ChristChurchApp
//
//  Created by Bose on 29/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class CCLabel: UILabel {

    var isFontChanged: Bool = false

 
    override func awakeFromNib() {
        super.awakeFromNib()
        if isFontChanged == false {
            isFontChanged = true
            let fontSize = self.font.pointSize * screenSize.width/320.0
            self.font =  UIFont(name: "AppleSDGothicNeo-Regular", size: fontSize)
        }
        
    }

}
