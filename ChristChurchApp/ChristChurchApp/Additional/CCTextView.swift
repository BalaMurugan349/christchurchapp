//
//  CCTextView.swift
//  ChristChurchApp
//
//  Created by Bose on 31/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class CCTextView: UITextView {

    var isFontChanged: Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        if isFontChanged == false {
            isFontChanged = true
            let fontSize = (self.font != nil ? self.font!.pointSize : 16) * screenSize.width/320.0
            self.font =  UIFont(name: "AppleSDGothicNeo-Regular", size: fontSize)
            self.selectable = false
        }
        
    }

}
