//
//  BMAnnotation.swift
//  Pickitup
//
//  Created by Bala on 31/12/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit
import MapKit


class BMAnnotation: MKPointAnnotation {

    var imageName: String!
    var tag : Int!
    var distance : String!
    
}
