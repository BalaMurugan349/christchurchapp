//
//  VideosTableViewCell.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/11/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class VideosTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewThumbnail : UIImageView!
    @IBOutlet weak var labelTitle : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelCategory : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
