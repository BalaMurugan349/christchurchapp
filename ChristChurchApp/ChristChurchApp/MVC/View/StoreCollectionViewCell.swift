//
//  StoreCollectionViewCell.swift
//  ChristChurchApp
//
//  Created by Bose on 30/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class StoreCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var labelProductName: CCLabel!
    
    func configureCell(product: Product) {
        imageViewProduct.image(product.imageUrl)
        labelProductName.text = product.name
    }
}
