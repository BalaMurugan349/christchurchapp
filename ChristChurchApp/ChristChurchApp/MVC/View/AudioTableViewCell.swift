//
//  AudioTableViewCell.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/15/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class AudioTableViewCell: UITableViewCell {

    @IBOutlet weak var labelFileName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelDuration : UILabel!
    @IBOutlet weak var labelSize : UILabel!
    @IBOutlet weak var buttonExpand : UIButton!
    @IBOutlet weak var buttonDelete : UIButton!
    @IBOutlet weak var buttonMail : UIButton!
    @IBOutlet weak var buttonPlay : UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(audioObj : Audio)  {
        labelFileName.text = audioObj.name
        labelFileName.textColor = audioObj.isExpanded == false ? UIColor.blackColor() : UIColor(red: 0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        labelDate.text = audioObj.createdDate
        labelDate.textColor = audioObj.isExpanded == false ? UIColor.darkGrayColor() : UIColor.whiteColor()
        labelDuration.text = audioObj.duration
        labelDuration.textColor = audioObj.isExpanded == false ? UIColor.darkGrayColor() : UIColor.whiteColor()
//        buttonExpand.setTitleColor(audioObj.isExpanded == false ? UIColor.blackColor() : UIColor.whiteColor(), forState: UIControlState.Normal)
        labelSize.text = audioObj.fileSize
        labelSize.textColor = audioObj.isExpanded == false ? UIColor.darkGrayColor() : UIColor.whiteColor()
        buttonExpand.hidden = audioObj.isExpanded == true
        self.backgroundColor = audioObj.isExpanded == true ? UIColor(red: 50.0/255.0, green: 50.0/255.0, blue:50.0/255.0, alpha: 1.0) :  UIColor(red: 236.0/255.0, green: 234.0/255.0, blue:235.0/255.0, alpha: 1.0)
        self.contentView.backgroundColor = audioObj.isExpanded == true ? UIColor(red: 50.0/255.0, green: 50.0/255.0, blue:50.0/255.0, alpha: 1.0) : UIColor(red: 236.0/255.0, green: 234.0/255.0, blue:235.0/255.0, alpha: 1.0)
        self.clipsToBounds = true

    }

}
