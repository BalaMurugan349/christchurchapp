//
//  ProgramTableViewCell.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/14/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class ProgramTableViewCell: UITableViewCell {
    @IBOutlet weak var labelTime : UILabel!
    @IBOutlet weak var labelProgram : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func configureCell (program : TVProgram){
        labelTime.text = program.programTime
        labelProgram.text = program.programTitle
    }
}
