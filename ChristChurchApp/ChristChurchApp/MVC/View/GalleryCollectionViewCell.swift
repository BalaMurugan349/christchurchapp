//
//  GalleryCollectionViewCell.swift
//  ChristChurchApp
//
//  Created by Bose on 31/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewAlbum: UIImageView!
    @IBOutlet weak var labelAlbumName: CCLabel!
    
    
    func configureCell(album: Album) {
        imageViewAlbum.image(album.coverImageUrl)
        labelAlbumName.text = album.title
    }
}