//
//  BMPageControl.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/11/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class BMPageControl: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var TotalPages : Int = 0 {
        willSet {
            
        } didSet {
            initView()
            self.frame = CGRectMake(0, 0, (CGFloat(TotalPages) * 20) + (CGFloat(TotalPages) * 10)  , 10)
            for i in 0...TotalPages - 1{
                let viewPage : UIView = UIView(frame: CGRectMake((CGFloat(i) * 20) + (CGFloat(i) * 10), 5, 20, 5))
                viewPage.backgroundColor = UIColor(red: 11.0/255.0, green: 17.0/255.0, blue: 27.0/255.0, alpha: 0.3)
                viewPage.tag = i
                self.addSubview(viewPage)
                
            }

        }
    }
    
    var selectedIndex : Int = 0 {
        willSet {
            for vw in self.subviews{
                if vw.tag == selectedIndex{
//                    vw.frame = CGRectMake((CGFloat(vw.tag) * 20) + (CGFloat(vw.tag) * 10), 5, 20, 5)
                    vw.backgroundColor = UIColor(red: 11.0/255.0, green: 17.0/255.0, blue: 27.0/255.0, alpha: 1.0)

                }else{
//                    vw.frame = CGRectMake((CGFloat(vw.tag) * 20) + (CGFloat(vw.tag) * 10), 9, 20, 1)
                    vw.backgroundColor = UIColor(red: 11.0/255.0, green: 17.0/255.0, blue: 27.0/255.0, alpha: 0.3)


                }
            }
        } didSet {
            
        }
    }


    func initView()  {
        self.userInteractionEnabled = false
        self.backgroundColor = UIColor.clearColor()
        self.frame = CGRectMake(0, 0, 0, 10)
    }
}
