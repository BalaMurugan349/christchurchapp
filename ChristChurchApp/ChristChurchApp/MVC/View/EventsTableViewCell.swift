//
//  EventsTableViewCell.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/13/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class EventsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewEvent : UIImageView!
    @IBOutlet weak var labelTitle : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var viewBackground : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell (event : Events){
        viewBackground.layer.cornerRadius = 3.0
        viewBackground.clipsToBounds = true
        imageViewEvent.image(event.eventImage)
        labelTitle.text = event.eventTitle
        labelTitle.font = IS_IPHONE6PLUS ? UIFont.systemFontOfSize(15) : UIFont.systemFontOfSize(13)
        labelDate.font = IS_IPHONE6PLUS ? UIFont.systemFontOfSize(13) : UIFont.systemFontOfSize(11)
        labelDate.text = formatDate(event.eventStartDate) + " - " + formatDate(event.eventEndDate)
    
    }
    
    func formatDate(strDate : String) -> String  {
        let dateFormatter : NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateOriginal = dateFormatter.dateFromString(strDate)
        dateFormatter.dateFormat = "MMM dd,yyyy hh:mm a"
        let strConvertedDate = dateFormatter.stringFromDate(dateOriginal!)
        return strConvertedDate
        
    }

}
