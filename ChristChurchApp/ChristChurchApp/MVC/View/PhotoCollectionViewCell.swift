//
//  PhotoCollectionViewCell.swift
//  ChristChurchApp
//
//  Created by Bose on 31/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewPhoto: UIImageView!
    
    
    func configureCell(photo: Photo) {
        imageViewPhoto.image(photo.imageUrl)
    }
}