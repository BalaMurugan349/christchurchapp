//
//  Product.swift
//  ChristChurchApp
//
//  Created by Bose on 30/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class Product: NSObject {
    
    var productId: String!
    var price: String!
    var name: String!
    var imageUrl: String!
    var desc: String!
    var createdDate: String!
    
    init(productDetails : [String: AnyObject]) {
        super.init()
        productId = productDetails["product_id"] as! String
        price = productDetails["price"] as! String
        name = productDetails["name"] as! String
        imageUrl = productDetails["image"] as! String
        desc = productDetails["desc"] as! String
        createdDate = productDetails["creation_date"] as! String
    }
    
    class func getAllProductObjects(arrayObjects : [NSDictionary]?) -> [Product]? {
        guard let arrayObjects = arrayObjects where  arrayObjects.count > 0 else {
            // Value requirements not met, do something
            return nil
        }
        var products = [Product]()
        for dict in arrayObjects {
            let product = Product(productDetails: dict as! [String: AnyObject])
            products.append(product)
        }
        return products
    }
    

    class func fetchAllProducts(page : Int, success:(result : AnyObject, nextPage: Int) -> Void, failure :(error : NSError?) -> Void) {
        let params = ["limit" : "10", "page" : "\(page)"]
        ServiceManager.fetchDataFromService("public/api/products?", parameters: params, success: { (result) in
            if (result["status"] as! Bool) == true {
                let pagination = result["pagination"] as? [String: AnyObject]
                let products = Product.getAllProductObjects(result["products"] as? [NSDictionary])
                if let _ = products {
                    let nextPage = pagination!["nextPage"] as! Int
                    success(result: products!, nextPage: nextPage)
                } else {
                    failure(error: nil)
                }
            }
        }) { (error) in
            failure(error: error)
        }
    }
}
