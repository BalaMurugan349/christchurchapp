//
//  Thoughts.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/10/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class Thoughts: NSObject {
    var thouhgtId: String!
    var thoughtTitle: String!
    var thoughtDate: String!
    
    init(thoughtDetails : [String: AnyObject]) {
        super.init()
        thouhgtId = thoughtDetails["thought_id"] as! String
        thoughtTitle = thoughtDetails["title"] as! String
        thoughtDate = "\(thoughtDetails["creation_date"]!)"
    }
    
    class func getAllThoughtObjects(arrayObjects : [NSDictionary]?) -> [Thoughts]? {
        guard let arrayObjects = arrayObjects where  arrayObjects.count > 0 else {
            // Value requirements not met, do something
            return nil
        }
        var thought = [Thoughts]()
        for dict in arrayObjects {
            let product = Thoughts(thoughtDetails: dict as! [String: AnyObject])
            thought.append(product)
        }
        return thought
    }
    
    
    class func fetchrecentThoughts(success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        let params = ["limit" : "10", ]
        ServiceManager.fetchDataFromService("public/api/thoughts?", parameters: params, success: { (result) in
            if (result["status"] as! Bool) == true {
                let products = Thoughts.getAllThoughtObjects(result["thoughts"] as? [NSDictionary])
                if let _ = products {
                    success(result: products!)
                } else {
                    failure(error: nil)
                }
            }
        }) { (error) in
            failure(error: error)
        }
    }
    
    
    class func fetchAllThoughts(page : Int, success:(result : AnyObject, nextPage: Int) -> Void, failure :(error : NSError?) -> Void) {
        let params = ["limit" : "10", "page" : "\(page)"]
        ServiceManager.fetchDataFromService("public/api/thoughts?", parameters: params, success: { (result) in
            if (result["status"] as! Bool) == true {
                let pagination = result["pagination"] as? [String: AnyObject]
                let products = Thoughts.getAllThoughtObjects(result["thoughts"] as? [NSDictionary])
                if let _ = products {
                    let nextPage = pagination!["nextPage"] as! Int
                    success(result: products!, nextPage: nextPage)
                } else {
                    failure(error: nil)
                }
            }
        }) { (error) in
            failure(error: error)
        }
    }
    
}
