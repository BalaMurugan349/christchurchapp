//
//  CCGeneral.swift
//  ChristChurchApp
//
//  Created by Bose on 29/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class CCGeneral: NSObject {
    class func fetchBackgroundImage(success:(result : AnyObject) -> Void, failure :(error : NSError) -> Void) {
        ServiceManager.fetchDataFromService("public/api/backgrounds", parameters: nil, success: { (result) in
            if (result["status"] as! Bool) == true {
                let images = result["Images"] as? [NSDictionary]
                let image = images?.first as? [String: String]
                if let img = image {
                    success(result: img["image"]!)
                }
            }
            }) { (error) in
                failure(error: error)
        }
    }
    
    
    //MARK:- FETCH LIVE TV URL
    class func fetchLiveTvUrl(success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        ServiceManager.fetchDataFromService("public/api/settings?", parameters: nil, success: { (result) in
            if (result["status"] as! Bool) == true {
                    success(result: result["settings"] as! NSDictionary)
                } else {
                    failure(error: nil)
                }
        }) { (error) in
            failure(error: error)
        }
    }
    
    
    //MARK:- FETCH FLASH NEWS
    class func fetchFlashNews(success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        ServiceManager.fetchDataFromService("public/api/flashnews?", parameters: nil, success: { (result) in
            if (result["status"] as! Bool) == true {
                success(result: result["news"] as! NSArray)
            } else {
                failure(error: nil)
            }
        }) { (error) in
            failure(error: error)
        }
    }
    
    //MARK:- ADD DEVICE TOKEN
    class func addDeviceToken(token : String, success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        let params = ["type" : "Ios", "token" : token, "device_id" : ""]
        ServiceManager.fetchDataFromPostService("public/api/addtoken?", parameters: params, success: { (result) in
            if (result["status"] as! Bool) == true {
                success(result: result["message"]!!)
            } else {
                failure(error: nil)
            }
        }) { (error) in
            failure(error: error)
        }
    }



}
