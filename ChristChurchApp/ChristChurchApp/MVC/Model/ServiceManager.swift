//
//  ServiceManager.swift
//  REHAB
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016 REHAB. All rights reserved.
//

import UIKit
import Alamofire

public let mainUrl = "http://christcentretv.in/appadmin/"

class ServiceManager: NSObject {
    
    class func fetchDataFromService(serviceName: String, parameters : [String : String]?, success:(result : AnyObject) -> Void, failure :(error : NSError) -> Void) {
        Alamofire.request(.GET, "\(mainUrl)\(serviceName)", parameters: parameters)
            .responseJSON { response in
                
                let result = response.result.value
                let error = response.result.error
//                let string = String(data: response.data!, encoding: NSUTF8StringEncoding)
//                print(string)
                if result != nil && error == nil {
                    success(result: result!)
                } else if error != nil {
                    failure(error: error!)
                } else {
                    failure(error: NSError(errorMessage: "Something went wrong"))
                }
        }
    }
    
    
    
    class func fetchDataFromPostService(serviceName: String, parameters : [String : String]?, success:(result : AnyObject) -> Void, failure :(error : NSError) -> Void) {
        Alamofire.request(.POST, "\(mainUrl)\(serviceName)", parameters: parameters)
            .responseJSON { response in
                
                let result = response.result.value
                let error = response.result.error
                //                let string = String(data: response.data!, encoding: NSUTF8StringEncoding)
                //                print(string)
                if result != nil && error == nil {
                    success(result: result!)
                } else if error != nil {
                    failure(error: error!)
                } else {
                    failure(error: NSError(errorMessage: "Something went wrong"))
                }
        }
    }

}
