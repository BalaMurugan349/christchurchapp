//
//  TVProgram.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/13/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class TVProgram: NSObject {
    
    var programDate : String!
    var programTitle : String!
    var programTime : String!
    
    init(programDetails : [String: AnyObject]) {
        super.init()
        programDate = programDetails["date"] == nil ? "No Data" :programDetails["date"] as! String
        programTitle = programDetails["title"] == nil ? "No Data" : programDetails["title"] as! String
        programTime = programDetails["time"] == nil ? "No Data" : programDetails["time"] as! String
    }

    class func getAllProgramObjects(objects : NSDictionary?) -> [[TVProgram]]? {
       if let dictObjects = objects {
        var program = [[TVProgram]]()
        (dictObjects.allKeys as NSArray).enumerateObjectsUsingBlock({ (obj, idx, stop) in
            let array : NSArray = dictObjects["\(idx)"] as! NSArray
            var prg = [TVProgram]()
            for dict in array {
                let product = TVProgram(programDetails: dict as! [String: AnyObject])
                prg.append(product)
            }
            program.append(prg)
            
        })
        return program
        }
        return nil
    }

    
    class func fetchPrograms(success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        ServiceManager.fetchDataFromService("public/api/schedule?", parameters: nil, success: { (result) in
            if (result["status"] as! Bool) == true {
                let products = TVProgram.getAllProgramObjects(result["schedule"] as? NSDictionary)
                if let _ = products {
                    success(result: products!)
                } else {
                    failure(error: nil)
                }
            }
        }) { (error) in
            failure(error: error)
        }
    }


}
