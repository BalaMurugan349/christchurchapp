//
//  Gallery.swift
//  ChristChurchApp
//
//  Created by Bose on 30/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class Album: NSObject {
    
    var albumId: String!
    var coverImageUrl: String!
    var thumbImageUrl: String!
    var title: String!
    var desc: String!
    var createdDate: String!
    
    init(albumDetails : [String: AnyObject]) {
        super.init()
        albumId = albumDetails["album_id"] as! String
        coverImageUrl = albumDetails["cover_image_main"] as! String
        thumbImageUrl = albumDetails["cover_image_thumb"] as! String
        title = albumDetails["title"] as! String
        desc = albumDetails["description"] as! String
        createdDate = "\(albumDetails["creation_date"])"
    }
    
    class func getAllAlbumObjects(arrayObjects : [NSDictionary]?) -> [Album]? {
        guard let arrayObjects = arrayObjects where  arrayObjects.count > 0 else {
            // Value requirements not met, do something
            return nil
        }
        var albums = [Album]()
        for dict in arrayObjects {
            let album = Album(albumDetails: dict as! [String: AnyObject])
            albums.append(album)
        }
        return albums
    }
    
    
    class func fetchAllAlbums(page : Int, success:(result : AnyObject, nextPage: Int) -> Void, failure :(error : NSError?) -> Void) {
        let params = ["limit" : "10", "page" : "\(page)"]
        ServiceManager.fetchDataFromService("public/api/albums?", parameters: params, success: { (result) in
            if (result["status"] as! Bool) == true {
                let pagination = result["pagination"] as? [String: AnyObject]
                let albums = Album.getAllAlbumObjects(result["albums"] as? [NSDictionary])
                if let _ = albums {
                    let nextPage = pagination!["nextPage"] as! Int
                    success(result: albums!, nextPage: nextPage)
                } else {
                    failure(error: nil)
                }
            }
        }) { (error) in
            failure(error: error)
        }
    }
}



//  Photo
//  ChristChurchApp
//
//  Created by Bose on 30/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

class Photo: NSObject {

    var imageId: String!
    var imageUrl: String!
    var thumbImageUrl: String!
    var title: String!
    var desc: String!
    var createdDate: String!
    
    init(photoDetails : [String: AnyObject]) {
        super.init()
        imageId = photoDetails["image_id"] as! String
        imageUrl = photoDetails["image_main"] as! String
        thumbImageUrl = photoDetails["image_thumb"] as! String
        title = photoDetails["title"] as! String
        desc = photoDetails["description"] as! String
        createdDate = "\(photoDetails["creation_date"])"
    }
    
    class func getAllPhotoObjects(arrayObjects : [NSDictionary]?) -> [Photo]? {
        guard let arrayObjects = arrayObjects where  arrayObjects.count > 0 else {
            // Value requirements not met, do something
            return nil
        }
        var photos = [Photo]()
        for dict in arrayObjects {
            let photo = Photo(photoDetails: dict as! [String: AnyObject])
            photos.append(photo)
        }
        return photos
    }
    
    
    class func fetchAllPhotos(page : Int, albumId: String, success:(result : AnyObject, nextPage: Int) -> Void, failure :(error : NSError?) -> Void) {
        let params = ["limit" : "10", "page" : "\(page)", "album_id" : albumId]
        ServiceManager.fetchDataFromService("public/api/images?", parameters: params, success: { (result) in
            if (result["status"] as! Bool) == true {
                let pagination = result["pagination"] as? [String: AnyObject]
                let photos = Photo.getAllPhotoObjects(result["images"] as? [NSDictionary])
                if let _ = photos {
                    let nextPage = pagination!["nextPage"] as! Int
                    success(result: photos!, nextPage: nextPage)
                } else {
                    failure(error: nil)
                }
            }
        }) { (error) in
            failure(error: error)
        }
    }
    
    
}
