//
//  Videos.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/11/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class Videos: NSObject {
    
    var videoId: String!
    var videoTitle: String!
    var videoCategoryId : String!
    var videoCategoryName : String!
    var videoUrl : String!
    var videoThumbnailUrl : String!
    var videoDate: String!
    
    init(videoDetails : [String: AnyObject]) {
        super.init()
        videoId = videoDetails["video_id"] as! String
        videoTitle = videoDetails["title"] as! String
        videoCategoryId = videoDetails["category_id"] as! String
        videoCategoryName = videoDetails["category_name"] as! String
        videoUrl = videoDetails["video_url"] as! String
        videoThumbnailUrl = videoDetails["image"] as! String
        videoDate = "\(videoDetails["creation_date"]!)"
        
        
    }
    
    class func getAllVideoObjects(arrayObjects : [NSDictionary]?) -> [Videos]? {
        guard let arrayObjects = arrayObjects where  arrayObjects.count > 0 else {
            // Value requirements not met, do something
            return nil
        }
        var video = [Videos]()
        for dict in arrayObjects {
            let product = Videos(videoDetails: dict as! [String: AnyObject])
            video.append(product)
        }
        return video
    }
    
    
    class func fetchrecentVideos(success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        let params = ["limit" : "10", "recent" : "1" ]
        ServiceManager.fetchDataFromService("public/api/videos?", parameters: params, success: { (result) in
            if (result["status"] as! Bool) == true {
                let products = Videos.getAllVideoObjects(result["videos"] as? [NSDictionary])
                if let _ = products {
                    success(result: products!)
                } else {
                    failure(error: nil)
                }
            }
        }) { (error) in
            failure(error: error)
        }
    }
    
    
    class func fetchAllCategories(success:(result : AnyObject) -> Void, failure :(error : NSError?) -> Void) {
        ServiceManager.fetchDataFromService("public/api/videocategories?", parameters: nil, success: { (result) in
            if (result["status"] as! Bool) == true {
                let products = Categories.getAllCategoryObjects(result["categories"] as? [NSDictionary])
                if let _ = products {
                    success(result: products!)
                } else {
                    failure(error: nil)
                }
            }
        }) { (error) in
            failure(error: error)
        }
    }
    
    class func fetchCategoryVideos(page : Int,categoryId : String, success:(result : AnyObject, nextPage: Int) -> Void, failure :(error : NSError?) -> Void) {
        let params = ["limit" : "10", "page" : "\(page)","category_id" : categoryId]
        ServiceManager.fetchDataFromService("public/api/videos?", parameters: params, success: { (result) in
            if (result["status"] as! Bool) == true {
                let pagination = result["pagination"] as? [String: AnyObject]
                let products = Videos.getAllVideoObjects(result["videos"] as? [NSDictionary])
                if let _ = products {
                    let nextPage = pagination!["nextPage"] as! Int
                    success(result: products!, nextPage: nextPage)
                } else {
                    failure(error: nil)
                }
            }
        }) { (error) in
            failure(error: error)
        }
    }
    
    
}

class Categories: NSObject {
    
    var categoryId: String!
    var categoryName: String!
    var categoryImage : String!
    var categoryDescription : String!
    
    init(videoDetails : [String: AnyObject]) {
        super.init()
        categoryId = videoDetails["category_id"] as! String
        categoryName = videoDetails["name"] as! String
        categoryImage = videoDetails["image"] as! String
        categoryDescription = videoDetails["description"] as! String
        
    }
    
    class func getAllCategoryObjects(arrayObjects : [NSDictionary]?) -> [Categories]? {
        guard let arrayObjects = arrayObjects where  arrayObjects.count > 0 else {
            // Value requirements not met, do something
            return nil
        }
        var video = [Categories]()
        for dict in arrayObjects {
            let product = Categories(videoDetails: dict as! [String: AnyObject])
            video.append(product)
        }
        return video
    }
    
    
}