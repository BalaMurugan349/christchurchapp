//
//  Events.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/13/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class Events: NSObject {

    var eventId : String!
    var eventTitle : String!
    var eventDescription : String!
    var eventImage : String!
    var eventStartDate : String!
    var eventEndDate : String!
    var eventCreationDate : String!
    
    init(eventDetails : [String: AnyObject]) {
        super.init()
        eventId = eventDetails["event_id"] as! String
        eventTitle = eventDetails["title"] as! String
        eventDescription = eventDetails["description"] as! String
        eventImage = eventDetails["image"] as! String
        eventStartDate = "\(eventDetails["start_date"]!)"
        eventEndDate = "\(eventDetails["end_date"]!)"
        eventCreationDate = "\(eventDetails["creation_date"]!)"
    }
    
    class func getAllEventObjects(arrayObjects : [NSDictionary]?) -> [Events]? {
        guard let arrayObjects = arrayObjects where  arrayObjects.count > 0 else {
            // Value requirements not met, do something
            return nil
        }
        var event = [Events]()
        for dict in arrayObjects {
            let product = Events(eventDetails: dict as! [String: AnyObject])
            event.append(product)
        }
        return event
    }


    class func fetchEvents(page : Int, success:(result : AnyObject, nextPage: Int) -> Void, failure :(error : NSError?) -> Void) {
        let params = ["limit" : "10", "page" : "\(page)"]
        ServiceManager.fetchDataFromService("public/api/events?", parameters: params, success: { (result) in
            if (result["status"] as! Bool) == true {
                let pagination = result["pagination"] as? [String: AnyObject]
                let products = Events.getAllEventObjects(result["events"] as? [NSDictionary])
                if let _ = products {
                    let nextPage = pagination!["nextPage"] as! Int
                    success(result: products!, nextPage: nextPage)
                } else {
                    failure(error: nil)
                }
            }
        }) { (error) in
            failure(error: error)
        }
    }

}
