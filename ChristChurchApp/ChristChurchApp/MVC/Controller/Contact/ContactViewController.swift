//
//  ContactViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/27/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit
import MapKit

class ContactViewController: CCTableViewController {

    var mapView: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Contact"
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "MenuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(toggleSideMenu(_:)))
        self.navigationItem.leftBarButtonItem = leftButton
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func toggleSideMenu(sender: AnyObject)
    {
        sideMenuController.setDrawerState(.Opened, animated: true)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return screenSize.width - 100
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView : UIView = UIView (frame: CGRectMake(0, 0, screenSize.width, screenSize.width - 100))
        headerView.backgroundColor = UIColor(red: 242.0/255.0, green: 239.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        
        mapView = mapView == nil ? MKMapView(frame: CGRectMake(0, 0, screenSize.width, screenSize.width - 100)) : mapView
        mapView.showsUserLocation = true
        mapView.delegate = self
        self.mapView.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true);
        headerView.addSubview(mapView)
        let annotaionObbj : BMAnnotation = BMAnnotation()
        annotaionObbj.imageName = "MapPin"
        annotaionObbj.title = "Christ Centre"
        annotaionObbj.coordinate = CLLocationCoordinate2DMake(8.5220, 76.9629)
        self.mapView.addAnnotation(annotaionObbj)
        let latDelta:CLLocationDegrees = 0.05
        let lonDelta:CLLocationDegrees = 0.05
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(8.5220, 76.9629)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        self.mapView.setRegion(region, animated: true)

        return headerView
    }

}

extension ContactViewController : MKMapViewDelegate{
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is BMAnnotation) {
            return nil
        }
        
        let reuseId = "Annotation"
        
        var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.canShowCallout = false
        }
        else {
            anView!.annotation = annotation
        }
        
        //Set annotation-specific properties **AFTER**
        //the view is dequeued or created...
        
        let cpa = annotation as! BMAnnotation
        anView!.image = UIImage(named:cpa.imageName)
        
        return anView
    }
}
