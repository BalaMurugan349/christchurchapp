//
//  StoreCollectionViewController.swift
//  ChristChurchApp
//
//  Created by Bose on 29/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

private let reuseIdentifier = "productsCell"

class StoreCollectionViewController: CCCollectionViewController {

    var nextPage = 1
    var isLoadingMoreData = true
    var isFromSideMenu = false
    var products: [Product] = [Product]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Store"
        self.setUI()
        self.getAllProducts()
    }
    
    func setUI() {
        if isFromSideMenu == true {
            let leftButton = UIBarButtonItem(image: UIImage(named: "MenuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(toggleSideMenu(_:)))
            self.navigationItem.leftBarButtonItem = leftButton
        }
    }

    func toggleSideMenu(sender: AnyObject)
    {
        sideMenuController.setDrawerState(.Opened, animated: true)
    }
    
    func getAllProducts() {
        self.activityIndicator.startAnimating()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {

        Product.fetchAllProducts(self.nextPage, success: { (result, nextPage) in
            self.nextPage = nextPage
            self.isLoadingMoreData = false
            self.products.appendContentsOf(result as! [Product])
            dispatch_async(dispatch_get_main_queue(), { 
                self.collectionView?.reloadData()
                self.activityIndicator.stopAnimating()

            })
        }) { (error) in
            dispatch_async(dispatch_get_main_queue(), {
                self.activityIndicator.stopAnimating()
            })
            if error != nil {
                self.isLoadingMoreData = false
                
            }
        }
        })
        
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
        {
            if (!self.isLoadingMoreData)
            {
                self.isLoadingMoreData = true
                self.getAllProducts()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return products.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! StoreCollectionViewCell
        let product = products[indexPath.item]
        cell.configureCell(product)
        return cell
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = (screenSize.width - 30)/2
        let height = (164 * screenSize.width)/320
        return CGSizeMake(width, height)
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let product = products[indexPath.item]
        let productDetailsVC = self.storyboard?.instantiateViewControllerWithIdentifier(kProductDetailsVC) as! ProductDetailsViewController
//        product.name = "Bsdsaa asdsdasdsads  sads  dsadsa sda dsa asdd sasdss"
//        product.desc = "sadsa ddsa dsa ds ads ads ad sad sad s dssaddasdsadsads sad dsa ds ad sds as adsa dsa dsa das ds ds ds a ds sassadasdas das das das das  sas as a sas ad ss dsa dsa d sa sa sd ad sa dsad sa dsadsadsa sd dsa sad s asda s dasda sd asd ad saa  sda sa a sda sd ads ads  sda sda s sd sda sad sda dsa ds dsa dsa dsa dsa dsa sda ds ad sa sdsd a sd sad sda sda sad s ad sdas da sad dsa"

        productDetailsVC.product = product
        self.navigationController?.pushViewController(productDetailsVC, animated: true)
        
    }

}
