//
//  PhotoDetailsViewController.swift
//  ChristChurchApp
//
//  Created by Bose on 31/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class PhotoDetailsViewController: CCViewController {

    var photo: Photo!
    var arrayPhotos : [Photo]!
    var startIndex : Int!
    
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var labelTitle: CCLabel!
    @IBOutlet weak var labelDescription: CCLabel!
    @IBOutlet weak var viewBackground : UIView!
    @IBOutlet weak var scrollImg : UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewPhoto.image(photo.imageUrl)
        labelTitle.text = photo.title
        labelDescription.text = photo.desc
        viewBackground.hidden = photo.title == "" && photo.desc == ""

        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
        scrollImg.alwaysBounceVertical = false
        scrollImg.alwaysBounceHorizontal = false
        scrollImg.showsVerticalScrollIndicator = true
        scrollImg.flashScrollIndicators()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        scrollImg.zoomScale = 1.0

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.imageViewPhoto
    }

}
