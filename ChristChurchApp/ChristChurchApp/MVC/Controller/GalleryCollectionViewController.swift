//
//  GalleryCollectionViewController.swift
//  ChristChurchApp
//
//  Created by Bose on 30/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit


class GalleryCollectionViewController: CCCollectionViewController {
    
    private let reuseIdentifier = "galleryCell"
    var nextPage = 1
    var isLoadingMoreData = true
    var isFromSideMenu = false
    var albums: [Album] = [Album]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Albums"
        self.setUI()
        self.getAllAlbums()
    }
    
    func setUI() {
        if isFromSideMenu == true {
            let leftButton = UIBarButtonItem(image: UIImage(named: "MenuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(toggleSideMenu(_:)))
            self.navigationItem.leftBarButtonItem = leftButton
        }
    }
    
    func toggleSideMenu(sender: AnyObject)
    {
        sideMenuController.setDrawerState(.Opened, animated: true)
    }
    
    func getAllAlbums() {
        self.activityIndicator.startAnimating()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            Album.fetchAllAlbums(self.nextPage, success: { (result, nextPage) in
                self.nextPage = nextPage
                self.isLoadingMoreData = false
                self.albums.appendContentsOf(result as! [Album])
                dispatch_async(dispatch_get_main_queue(), {
                    self.collectionView?.reloadData()
                    self.activityIndicator.stopAnimating()
                })
            }) { (error) in
                dispatch_async(dispatch_get_main_queue(), {
                    self.activityIndicator.stopAnimating()
                })
                if error != nil {
                    self.isLoadingMoreData = false
                }
            }
        })        
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
        {
            if (!self.isLoadingMoreData)
            {
                self.isLoadingMoreData = true
                self.getAllAlbums()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return albums.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! GalleryCollectionViewCell
        let album = albums[indexPath.item]
        cell.configureCell(album)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = screenSize.width - 20
        let height = (120 * screenSize.width)/320
        return CGSizeMake(width, height)
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let album = albums[indexPath.item]
        let photoCollectionVC = self.storyboard?.instantiateViewControllerWithIdentifier(kPhotoCollectionVC) as! PhotoCollectionViewController
        photoCollectionVC.album = album
        self.navigationController?.pushViewController(photoCollectionVC, animated: true)

    }

}
