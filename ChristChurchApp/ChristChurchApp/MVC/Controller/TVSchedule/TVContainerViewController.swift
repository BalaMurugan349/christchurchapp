//
//  TVContainerViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/13/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class TVContainerViewController: CCViewController {
    
    var isFromSideMenu = false
    var arrayPrograms : [[TVProgram]] = [[TVProgram]]()
    var arrayButtons : [UIButton] = [UIButton]()
    var arrayDays = ["","SUN","MON","TUE","WED","THU","FRI","SAT"]
    var pageController : UIPageViewController!
    var arrayVC : [UIViewController]! = [UIViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "TV Program"
        if isFromSideMenu == true {
            let leftButton = UIBarButtonItem(image: UIImage(named: "MenuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(toggleSideMenu(_:)))
            self.navigationItem.leftBarButtonItem = leftButton
        }
        setDateView()
        fetchTVPrograms()
        
    }
    
    func setDateView()  {
        for i in 0...6{
            let vw : UIView = UIView (frame:  CGRectMake((CGFloat(i) * (screenSize.width/7)) + (CGFloat(i) * 1) - 1, screenSize.height - 124, screenSize.width/7 -  1, 60))
            vw.backgroundColor = UIColor.lightGrayColor()
            let btn : UIButton = UIButton(type: .Custom)
            btn.setTitle(getButtonTitle(Double(i)), forState: UIControlState.Normal)
            btn.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            btn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Selected)
            btn.titleLabel?.font = UIFont.systemFontOfSize(15)
            btn.backgroundColor = i == 0 ? UIColor(red: 114.0/255.0, green: 138.0/255.0, blue: 149.0/255.0, alpha: 1.0) : UIColor.lightGrayColor()
            btn.selected = i == 0
            btn.frame = vw.bounds
            btn.titleLabel?.lineBreakMode = .ByWordWrapping
            btn.titleLabel?.numberOfLines = 0
            btn.titleLabel?.textAlignment = .Center
            btn.addTarget(self, action: #selector(TVContainerViewController.btnDateAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            btn.tag = i
            vw.addSubview(btn)
            arrayButtons.append(btn)
            self.view.addSubview(vw)
        }
        
    }
    
     func getButtonTitle (index : Double) -> String{
        let now : NSDate = NSDate()
        let newDate : NSDate = now.dateByAddingTimeInterval(60 * 60 * 24 * index + 1)
        let dateFormatter : NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let day = getDayOfWeek(dateFormatter.stringFromDate(newDate))
        dateFormatter.dateFormat = "dd"
        let strDate = dateFormatter.stringFromDate(newDate)
        return arrayDays[day] + "\n" + strDate
    }
    
    func getDayOfWeek(today:String)->Int {
        
        let formatter  = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let todayDate = formatter.dateFromString(today)!
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let myComponents = myCalendar.components(.Weekday, fromDate: todayDate)
        let weekDay = myComponents.weekday
        return weekDay
    }
    
    func btnDateAction(sender : UIButton)  {
        for btn in arrayButtons{
           btn.selected =  sender.tag == btn.tag
            btn.backgroundColor = sender.tag == btn.tag ? UIColor(red: 114.0/255.0, green: 138.0/255.0, blue: 149.0/255.0, alpha: 1.0) : UIColor.lightGrayColor()
        }
        
        if pageController != nil{
        let firstViewController = arrayVC[sender.tag]
        pageController.setViewControllers([firstViewController],
                                          direction: .Forward,
                                          animated: false,
                                          completion: nil)
        }

    }
    
    func setSelectedIndex (index : Int){
        for btn in arrayButtons{
            btn.selected =  index == btn.tag
            btn.backgroundColor = index == btn.tag ? UIColor(red: 114.0/255.0, green: 138.0/255.0, blue: 149.0/255.0, alpha: 1.0) : UIColor.lightGrayColor()
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func toggleSideMenu(sender: AnyObject)
    {
        sideMenuController.setDrawerState(.Opened, animated: true)
    }

    func fetchTVPrograms() {
        self.activityIndicator.startAnimating()
        TVProgram.fetchPrograms({ (result) in
            self.arrayPrograms.appendContentsOf(result as! [[TVProgram]])
            self.activityIndicator.stopAnimating()
            self.setPageViewController()
        }) { (error) in
            self.activityIndicator.stopAnimating()
    
        }
    }
    
   func setPageViewController (){
    pageController = self.childViewControllers.first as! UIPageViewController
    
    for (idx,prog) in arrayPrograms.enumerate(){
        let progVC = self.storyboard?.instantiateViewControllerWithIdentifier("ProgramVC") as! ProgramTableViewController
        progVC.view.tag = idx
        let prg : TVProgram = prog[0]
        progVC.progArray = prog.count == 1 && prg.programTitle == "No Data" ? [TVProgram]() : prog
        arrayVC.append(progVC)
    }
    
    pageController.dataSource = self
    pageController.delegate = self
    
    let firstViewController = arrayVC[0]
    pageController.setViewControllers([firstViewController],
                       direction: .Forward,
                       animated: true,
                       completion: nil)
    }
}


extension TVContainerViewController : UIPageViewControllerDelegate,UIPageViewControllerDataSource{
    
    func pageViewController(pageViewController: UIPageViewController,
                            viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = arrayVC.indexOf(viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard arrayVC.count > previousIndex else {
            return nil
        }
        return arrayVC[previousIndex]
    }
    
    func pageViewController(pageViewController: UIPageViewController,
                            viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = arrayVC.indexOf(viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = arrayVC.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        return arrayVC[nextIndex]
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if(completed) {
            let vc = pageViewController.viewControllers?.last
            setSelectedIndex((vc?.view.tag)!)
        }
        
    }
    
}
