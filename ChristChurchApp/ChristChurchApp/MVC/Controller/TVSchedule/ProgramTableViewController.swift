//
//  ProgramTableViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/14/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit
import EventKit

class ProgramTableViewController: UITableViewController {

    var progArray : [TVProgram]!
    var eventStore : EKEventStore = EKEventStore()
    var isAccessToEventStoreGranted : Bool = false
    var calendar : EKCalendar!
    var reminders : NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateAuthorizationStatus()
        fetchReminders()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProgramTableViewController.fetchReminders), name: EKEventStoreChangedNotification, object: nil)

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        if progArray.count == 0{
            let labelNoData : UILabel = UILabel(frame: CGRectMake(0, 0, screenSize.width, screenSize.height - 124))
            labelNoData.text = "No Data found !"
            labelNoData.font = UIFont.boldSystemFontOfSize(17)
            labelNoData.textAlignment = NSTextAlignment.Center
            self.view.addSubview(labelNoData)
            self.view.bringSubviewToFront(labelNoData)
            self.tableView.scrollEnabled = false
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return progArray.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! ProgramTableViewCell
       cell.configureCell(progArray[indexPath.row])
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let alertController = UIAlertController(title: "Christ Center", message: "Do you want to add this to your reminder", preferredStyle: UIAlertControllerStyle.Alert)
        let alertOkAction = UIAlertAction(title: "NO", style: UIAlertActionStyle.Default) { (action) -> Void in
            
        }
        let alertOkAction1 = UIAlertAction(title: "YES", style: UIAlertActionStyle.Default) { (action) ->
            Void in
            let prog : TVProgram = self.progArray[indexPath.row]
            let dateStr = prog.programDate + " " + prog.programTime
            let dateFormatter : NSDateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
            let dueDate : NSDate = dateFormatter.dateFromString(dateStr)!
            if self.didReminderExists(prog.programTitle){
                UIAlertView(title: "", message: "Reminder already exists", delegate: nil, cancelButtonTitle: "OK").show()
            }else{
                self.addReminder(prog.programTitle, dueDate: dueDate)
            }
        }
        alertController.addAction(alertOkAction)
        alertController.addAction(alertOkAction1)
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    
    //MARK:- EVENT KIT
    func updateAuthorizationStatus()  {
        let authorizationStatus : EKAuthorizationStatus = EKEventStore.authorizationStatusForEntityType(EKEntityType.Reminder)
        switch authorizationStatus {
        case EKAuthorizationStatus.Denied:
            break
        case EKAuthorizationStatus.Restricted:
            self.isAccessToEventStoreGranted = false
            UIAlertView(title: "Access Denied", message: "This app doesn't have access to your Reminders.", delegate: nil, cancelButtonTitle: "Ok").show()
            break
        case EKAuthorizationStatus.Authorized:
            self.isAccessToEventStoreGranted = true
            break
        case EKAuthorizationStatus.NotDetermined:
            self.eventStore.requestAccessToEntityType(EKEntityType.Reminder, completion: { (granted, error) in
                self.isAccessToEventStoreGranted = true
            })
            break
        }
    }
    
    //MARK:- INITIATE CALENDAR
    func calendarObj() -> EKCalendar {
        if calendar == nil{
            let calendars = self.eventStore.calendarsForEntityType(EKEntityType.Reminder)
            
            let calendarTitle = "ChristCenter"
            let predicate : NSPredicate = NSPredicate(format: "title matches %@", calendarTitle)
            let filtered : NSArray = (calendars as NSArray).filteredArrayUsingPredicate(predicate)
            
            if filtered.count > 0{
                calendar = filtered.firstObject as! EKCalendar
            }else{
                calendar = EKCalendar(forEntityType: .Reminder, eventStore: eventStore)
                calendar.title = "ChristCenter"
                calendar.source = self.eventStore.defaultCalendarForNewReminders().source
                
                do{
                    try self.eventStore.saveCalendar(calendar, commit: true)
                }catch{
                    
                }
            }
        }
        return calendar
    }
    
    //MARK:- ADD REMINDER
    func addReminder(title : String , dueDate : NSDate)  {
        if (!self.isAccessToEventStoreGranted) {
        return
        }
        
        let reminder : EKReminder = EKReminder(eventStore: self.eventStore)
        reminder.title = title
        reminder.calendar = self.calendarObj()
        let gregorian = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
        let unitFlags = NSCalendarUnit(rawValue: UInt.max)
        reminder.dueDateComponents = gregorian?.components(unitFlags, fromDate: dueDate)
        reminder.addAlarm(EKAlarm(absoluteDate: dueDate))
        
        do{
          try self.eventStore.saveReminder(reminder, commit: true)
        }catch{
            
        }
        UIAlertView(title: "", message: "Reminder was successfully added!.", delegate: nil, cancelButtonTitle: "OK").show()

        
    }
    
    //MARK:- FETCH REMINDERS
    func fetchReminders() {
        if (self.isAccessToEventStoreGranted) {
            let predicate : NSPredicate = self.eventStore.predicateForRemindersInCalendars([calendarObj()])
            self.eventStore.fetchRemindersMatchingPredicate(predicate, completion: { (reminders) in
                self.reminders = reminders!
            })
        }

    }
    
    func didReminderExists (title : String) -> Bool{
        let  predicate : NSPredicate = NSPredicate(format: "title matches %@", title)
        let filtered : NSArray = self.reminders.filteredArrayUsingPredicate(predicate)
        return self.isAccessToEventStoreGranted && filtered.count > 0
    }

}
