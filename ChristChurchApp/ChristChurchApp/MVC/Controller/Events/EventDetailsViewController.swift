//
//  EventDetailsViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/13/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class EventDetailsViewController: CCViewController {
    
    @IBOutlet weak var imageViewEvent : UIImageView!
    @IBOutlet weak var labelTitle : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var viewBackground : UIView!
    @IBOutlet weak var textviewDescription : UITextView!

    var event : Events!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Event Details"
        imageViewEvent.image(event.eventImage)
        labelTitle.text = event.eventTitle
        labelDate.text = formatDate(event.eventStartDate) + " - " + formatDate(event.eventEndDate)
        textviewDescription.text = event.eventDescription
        viewBackground.layer.cornerRadius = 3.0
        viewBackground.clipsToBounds = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func formatDate(strDate : String) -> String  {
        let dateFormatter : NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateOriginal = dateFormatter.dateFromString(strDate)
        dateFormatter.dateFormat = "MMM dd,yyyy hh:mm a"
        let strConvertedDate = dateFormatter.stringFromDate(dateOriginal!)
        return strConvertedDate
        
    }


}
