//
//  EventsViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/13/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class EventsViewController: CCTableViewController {

    var isFromSideMenu = false
    var arrayEvents: [Events] = [Events]()
    var nextPage = 1
    var isLoadingMoreData = true

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Events"
        if isFromSideMenu == true {
            let leftButton = UIBarButtonItem(image: UIImage(named: "MenuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(toggleSideMenu(_:)))
            self.navigationItem.leftBarButtonItem = leftButton
        }
        self.getAllEvents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func toggleSideMenu(sender: AnyObject)
    {
        sideMenuController.setDrawerState(.Opened, animated: true)
    }


    func getAllEvents()  {
        self.activityIndicator.startAnimating()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            Events.fetchEvents(self.nextPage, success: { (result, nextPage) in
                self.nextPage = nextPage
                self.isLoadingMoreData = false
                self.arrayEvents.appendContentsOf(result as! [Events])
                dispatch_async(dispatch_get_main_queue(), {
                    self.tableView.reloadData()
                    self.activityIndicator.stopAnimating()
                })
                
                }, failure: { (error) in
                    dispatch_async(dispatch_get_main_queue(), {
                        self.activityIndicator.stopAnimating()
                    })
                    if error != nil {
                        self.isLoadingMoreData = false
                    }
            })
        })
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
        {
            if (!self.isLoadingMoreData)
            {
                self.isLoadingMoreData = true
                self.getAllEvents()
            }
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayEvents.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return IS_IPHONE6 ? 250 : IS_IPHONE6PLUS ? 280 : 210
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! EventsTableViewCell
        cell.configureCell(arrayEvents[indexPath.row])
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let eventDetails : EventDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("EventDetailsVC") as! EventDetailsViewController
        eventDetails.event = arrayEvents[indexPath.row]
        self.navigationController?.pushViewController(eventDetails, animated: true)
    }
    



}
