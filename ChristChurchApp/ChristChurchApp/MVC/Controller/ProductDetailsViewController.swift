//
//  ProductDetailsViewController.swift
//  ChristChurchApp
//
//  Created by Bose on 31/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class ProductDetailsViewController: CCTableViewController {
    
    @IBOutlet weak var imageViewProductImage: UIImageView!
    @IBOutlet weak var labelProductName: CCLabel!
    @IBOutlet weak var labelPrice: CCLabel!
    @IBOutlet weak var textViewDescription: CCTextView!
    
    var product: Product!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        imageViewProductImage.image(product.imageUrl)
        labelProductName.text = product.name
        labelPrice.text = "\(product.price) INR"
        textViewDescription.text = product.desc
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var height : CGFloat = 25
        if indexPath.row == 0{
            height =  product.name.heightForLabel(UIFont.systemFontOfSize(22, weight: UIFontWeightMedium), width: screenSize.width - 40).height > 28 ? product.name.heightForLabel(UIFont.systemFontOfSize(20, weight: UIFontWeightMedium), width: screenSize.width - 40).height + 30 : 58
        }else if indexPath.row == 2{
            height =  product.desc.heightForLabel(UIFont.systemFontOfSize(14), width: screenSize.width - 40).height > 30 ? product.desc.heightForLabel(UIFont.systemFontOfSize(14), width: screenSize.width - 40).height + 25 : 55
        }
        
        return height
    }
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return screenSize.width - 50
    }

    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView : UIView = UIView (frame: CGRectMake(0, 0, screenSize.width, screenSize.width - 50))
        headerView.backgroundColor = UIColor.whiteColor()
        let imgvw : UIImageView = UIImageView(frame: CGRectMake(0, 0, screenSize.width, screenSize.width - 50))
        imgvw.image(product.imageUrl)
        headerView.addSubview(imgvw)
        imgvw.center = headerView.center
        return headerView
    }
}
