//
//  HomeViewController.swift
//  ChristChurchApp
//
//  Created by Bose on 29/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit


class HomeViewController: CCViewController {

    @IBOutlet weak var imageViewBackground: UIImageView!
    var tvUrl : String = ""
    var moviePlayer : MPMoviePlayerController!
    var isLiveTV : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setBackgroundImage()
        if isLiveTV == true {  fetchLiveTv() }
        // Do any additional setup after loading the view.
    }
    
    override func loadView() {
        super.loadView()
        let leftButton = UIBarButtonItem(image: UIImage(named: "MenuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(toggleSideMenu(_:)))
        self.navigationItem.leftBarButtonItem = leftButton
    }
    
    func setBackgroundImage() {
        CCGeneral.fetchBackgroundImage({ (result) in
            self.imageViewBackground.image(result as! String)
        }) { (error) in
            
        }
        self.setFlashNews()
    }
    
    //MARK:- SET FLASH NEWS
    func setFlashNews()  {
        CCGeneral.fetchFlashNews({ (result) in
            let arrayNews = (result as! NSArray).valueForKey("news")
            let strNews = arrayNews.componentsJoinedByString("          ")
            let labelScroll = LBScrollLabel(frame: CGRectMake(10, 0 , screenSize.width - 20, 30) )
            labelScroll.text = "          " + strNews
            labelScroll.textColor = UIColor.whiteColor()
            labelScroll.font = UIFont.systemFontOfSize(15)
            labelScroll.beginScrollWithDirection(LBScrollLabelDirection.Left)
            self.view.addSubview(labelScroll)
            }) { (error) in
                
        }
    }
    
    func toggleSideMenu(sender: AnyObject)
    {
        sideMenuController.setDrawerState(.Opened, animated: true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchLiveTv()  {
        self.view.userInteractionEnabled = false
        CCGeneral.fetchLiveTvUrl({ (result) in
            self.tvUrl = result["live_tv_url"] as! String
            self.view.userInteractionEnabled = true
            self.playLiveTV()
        }) { (error) in
            self.view.userInteractionEnabled = true
        }
    }
    
    func playLiveTV(){
        moviePlayer = MPMoviePlayerController(contentURL: NSURL(string: self.tvUrl))
        moviePlayer.scalingMode = MPMovieScalingMode.AspectFill
        moviePlayer.view.frame = self.view.bounds
        moviePlayer.movieSourceType = MPMovieSourceType.File
        moviePlayer.prepareToPlay()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewController.doneMoviePlayerButtonClicked(_:)), name: MPMoviePlayerWillExitFullscreenNotification, object: moviePlayer)
        moviePlayer.controlStyle = MPMovieControlStyle.Embedded
        moviePlayer.play()
        self.view.addSubview(moviePlayer.view)
        moviePlayer.fullscreen = true
        videosInFullScreen = true

    }
    func doneMoviePlayerButtonClicked(aNotification : NSNotification)  {
        videosInFullScreen = false
        let player : MPMoviePlayerController = aNotification.object as! MPMoviePlayerController
        player.stop()
        player.view.removeFromSuperview()
    }

    @IBAction func onSocialMediaButtonPressed (sender : UIButton){
        switch sender.tag {
        case 0:
            UIApplication.sharedApplication().openURL(NSURL(string: "https://www.facebook.com/Brother-Suresh-Babu-1509731029266901/")!)
            break
        case 1:
            UIApplication.sharedApplication().openURL(NSURL(string: "https://twitter.com/brosureshbabu")!)
            break
        case 2:
            UIApplication.sharedApplication().openURL(NSURL(string: "https://www.youtube.com/channel/UC8V18L0leapCTi41mSRvmgg/feed")!)
            break
        case 3:
            UIApplication.sharedApplication().openURL(NSURL(string: "http://sureshbabu.org/")!)
            break

        default:
            break
        }
    }

    @IBAction func buttonAction(sender: UIButton) {
        switch sender.tag {
        case 0:
            //tv
//            let video : VideoPlayerViewController = self.storyboard?.instantiateViewControllerWithIdentifier("VideoPlayerVC") as! VideoPlayerViewController
//            video.isLiveTV = true
//            self.navigationController?.pushViewController(video, animated: true)
             self.tvUrl == "" ? fetchLiveTv() : self.playLiveTV()

            break
        case 1:
            //prayer
            let prayer = self.storyboard?.instantiateViewControllerWithIdentifier("PrayerContainerVC") as! PrayerContainerViewController
            self.navigationController?.pushViewController(prayer, animated: true)

            break
        case 2:
            //thoughts
            let thoughts = self.storyboard?.instantiateViewControllerWithIdentifier("ThoughtsVC") as! ThoughtsViewController
            self.navigationController?.pushViewController(thoughts, animated: true)

            break
        case 3:
            //videos
            let videos = self.storyboard?.instantiateViewControllerWithIdentifier("VideosVC") as! VideosViewController
            self.navigationController?.pushViewController(videos, animated: true)

            break
        case 4:
            let storeCollectionVC = self.storyboard?.instantiateViewControllerWithIdentifier(kStoreCollectionVC) as! StoreCollectionViewController
            self.navigationController?.pushViewController(storeCollectionVC, animated: true)
            //store
            break
        default:
            let galleryCollectionVC = self.storyboard?.instantiateViewControllerWithIdentifier(kGalleryCollectionVC) as! GalleryCollectionViewController
            self.navigationController?.pushViewController(galleryCollectionVC, animated: true)
            //gallery
            break
        }
    }
    

}
