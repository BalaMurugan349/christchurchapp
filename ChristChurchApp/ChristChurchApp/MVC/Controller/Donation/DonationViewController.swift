//
//  DonationViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/27/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class DonationViewController: CCTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Donation"

        let leftButton = UIBarButtonItem(image: UIImage(named: "MenuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(toggleSideMenu(_:)))
        self.navigationItem.leftBarButtonItem = leftButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func toggleSideMenu(sender: AnyObject)
    {
        sideMenuController.setDrawerState(.Opened, animated: true)
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return screenSize.width - 100
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView : UIView = UIView (frame: CGRectMake(0, 0, screenSize.width, screenSize.width - 100))
        headerView.backgroundColor = UIColor(red: 242.0/255.0, green: 239.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        let imgvw : UIImageView = UIImageView(frame: CGRectMake(0, 0, 180, 180))
        imgvw.image = UIImage(named: "Donate")
        headerView.addSubview(imgvw)
        imgvw.center = headerView.center
        return headerView
    }

}
