//
//  PageViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/5/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {

    var arrayPhotos : [Photo]!
    var arrayVC : [UIViewController]! = [UIViewController]()
    var index : Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        for photo in arrayPhotos{
        let photoDetailsVC = self.storyboard?.instantiateViewControllerWithIdentifier(kPhotoDetailsVC) as! PhotoDetailsViewController
            photoDetailsVC.photo = photo
            arrayVC.append(photoDetailsVC)
        }
        
        self.dataSource = self
        self.delegate = self
        
        let firstViewController = arrayVC[index]
        
        setViewControllers([firstViewController],
                               direction: .Forward,
                               animated: false,
                               completion: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension PageViewController : UIPageViewControllerDelegate,UIPageViewControllerDataSource{
    
func pageViewController(pageViewController: UIPageViewController,
                        viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
    guard let viewControllerIndex = arrayVC.indexOf(viewController) else {
        return nil
    }
    
    let previousIndex = viewControllerIndex - 1
    
    guard previousIndex >= 0 else {
        return nil
    }
    
    guard arrayVC.count > previousIndex else {
        return nil
    }
    
    return arrayVC[previousIndex]
}

func pageViewController(pageViewController: UIPageViewController,
                        viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
    guard let viewControllerIndex = arrayVC.indexOf(viewController) else {
        return nil
    }
    
    let nextIndex = viewControllerIndex + 1
    let orderedViewControllersCount = arrayVC.count
    
    guard orderedViewControllersCount != nextIndex else {
        return nil
    }
    
    guard orderedViewControllersCount > nextIndex else {
        return nil
    }
    return arrayVC[nextIndex]
}
    
}