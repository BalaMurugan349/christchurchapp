//
//  MenuTableViewController.swift
//  ChristChurchApp
//
//  Created by Bose on 29/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case Home = 1
    case LiveTV = 2
    case PrayerRequest = 3
    case Videos = 4
    case Gallery = 5
    case Store = 6
    case Events = 7
    case Thoughts = 8
    case TVProgram = 9
    case Donation = 10
    case Contact = 12
    case AboutApp = 13
}



class MenuTableViewController : UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.row == 0 ? 50 : (screenSize.height - 50)/15
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let indexPaths = [0, 11, 14]
        if indexPaths.contains(indexPath.row) {
            let cell = tableView.dequeueReusableCellWithIdentifier("menuCell2", forIndexPath: indexPath)
            let view = cell.contentView.viewWithTag(1)
            view?.hidden = indexPath.row != 11
            let viewLogo = cell.contentView.viewWithTag(100)
            viewLogo?.hidden = indexPath.row != 0

            
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("menuCell1", forIndexPath: indexPath) as! MenuTableViewCell
            let titles = ["", "Home", "Live TV", "Prayer Request", "Videos", "Gallery", "Store", "Events", "Thoughts", "TV Programs", "Donations", "", "Contact Us", "About Us"]
            cell.labelMenuTitle.text = titles[indexPath.row]
            cell.imageViewIcon.image = UIImage(named: "Menu\(indexPath.row - 1)")
            return cell
        }
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let indexPaths = [0, 11, 14]
        if !indexPaths.contains(indexPath.row) {
            
            if let menu = LeftMenu(rawValue: indexPath.row) {
                self.changeViewController(menu)
            }
        }
    }
    
    func changeViewController(menu: LeftMenu) {
        switch menu {
        case .Home:
            let homeVC = self.storyboard?.instantiateViewControllerWithIdentifier(kHomeVC) as! HomeViewController
            homeVC.isLiveTV = false
            changeMainVC(homeVC)
            break
        case .LiveTV:
            let homeVC = self.storyboard?.instantiateViewControllerWithIdentifier(kHomeVC) as! HomeViewController
            homeVC.isLiveTV = true
            changeMainVC(homeVC)
//            let video : VideoPlayerViewController = self.storyboard?.instantiateViewControllerWithIdentifier("VideoPlayerVC") as! VideoPlayerViewController
//            video.isLiveTV = true
//            video.isFromSideMenu = true
//            changeMainVC(video)

            break
        case .PrayerRequest:
            let prayer = self.storyboard?.instantiateViewControllerWithIdentifier("PrayerContainerVC") as! PrayerContainerViewController
            prayer.isFromSideMenu = true
            changeMainVC(prayer)
            break
        case .Videos:
            let videos = self.storyboard?.instantiateViewControllerWithIdentifier("VideosVC") as! VideosViewController
            videos.isFromSideMenu = true
            changeMainVC(videos)

            break
        case .Gallery:
            let galleryCollectionVC = self.storyboard?.instantiateViewControllerWithIdentifier(kGalleryCollectionVC) as! GalleryCollectionViewController
            galleryCollectionVC.isFromSideMenu = true
            changeMainVC(galleryCollectionVC)
            break
        case .Store:
            let storeCollectionVC = self.storyboard?.instantiateViewControllerWithIdentifier(kStoreCollectionVC) as! StoreCollectionViewController
            storeCollectionVC.isFromSideMenu = true
            changeMainVC(storeCollectionVC)
            break
        case .Events:
            let eventsVC = self.storyboard?.instantiateViewControllerWithIdentifier("EventsVC") as! EventsViewController
            eventsVC.isFromSideMenu = true
            changeMainVC(eventsVC)

            break
        case .Thoughts:
            let thoughts = self.storyboard?.instantiateViewControllerWithIdentifier("ThoughtsVC") as! ThoughtsViewController
            thoughts.isFromSideMenu = true
            changeMainVC(thoughts)

            break
        case .TVProgram:
            let tv = self.storyboard?.instantiateViewControllerWithIdentifier("TVContainerVC") as! TVContainerViewController
            tv.isFromSideMenu = true
            changeMainVC(tv)

            break
        case .Donation:
            let donation = self.storyboard?.instantiateViewControllerWithIdentifier("DonationVC") as! DonationViewController
            changeMainVC(donation)

            break
        case .Contact:
            let contact = self.storyboard?.instantiateViewControllerWithIdentifier("ContactVC") as! ContactViewController
            changeMainVC(contact)

            break
        case .AboutApp:
            let About = self.storyboard?.instantiateViewControllerWithIdentifier("AboutVC") as! AboutUsViewController
            changeMainVC(About)

            break
        }
    }
    
    func changeMainVC(viewController : UIViewController)
    {
        sideMenuController.mainViewController = UINavigationController(rootViewController: viewController)
        sideMenuController.setDrawerState(.Closed, animated: true)
    }
}

