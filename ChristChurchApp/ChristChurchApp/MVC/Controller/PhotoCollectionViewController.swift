//
//  PhotoCollectionViewController.swift
//  ChristChurchApp
//
//  Created by Bose on 31/07/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit


class PhotoCollectionViewController: CCCollectionViewController {

    private let reuseIdentifier = "photoCell"
    var nextPage = 1
    var isLoadingMoreData = true
    var photos: [Photo] = [Photo]()
    var album: Album!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Photos"
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Register cell classes
        //self.collectionView!.registerClass(StoreCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.getAllPhotos()
    }
    
    
    
    func getAllPhotos() {
        self.activityIndicator.startAnimating()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            Photo.fetchAllPhotos(self.nextPage, albumId: self.album.albumId, success: { (result, nextPage) in
                self.nextPage = nextPage
                self.isLoadingMoreData = false
                self.photos.appendContentsOf(result as! [Photo])
                dispatch_async(dispatch_get_main_queue(), {
                    self.collectionView?.reloadData()
                    self.activityIndicator.stopAnimating()
                })
            }) { (error) in
                dispatch_async(dispatch_get_main_queue(), {
                    self.activityIndicator.stopAnimating()
                })
                if error != nil {
                    self.isLoadingMoreData = false
                }
            }
        })
        
        
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
        {
            if (!self.isLoadingMoreData)
            {
                self.isLoadingMoreData = true
                self.getAllPhotos()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return photos.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! PhotoCollectionViewCell
        let photo = photos[indexPath.item]
        cell.configureCell(photo)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = (screenSize.width - 30)/2
        let height = (120 * screenSize.width)/320
        return CGSizeMake(width, height)
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let pageVC : PageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageVC") as! PageViewController
        pageVC.arrayPhotos = photos
        pageVC.index = indexPath.row
        self.navigationController?.pushViewController(pageVC, animated: true)
        

    }
    
    
}
