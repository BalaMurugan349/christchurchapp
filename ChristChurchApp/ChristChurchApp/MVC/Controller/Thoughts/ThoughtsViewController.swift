//
//  ThoughtsViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/9/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class ThoughtsViewController: CCViewController {

    var arrayVC : [AnyObject]!
    var pageControl : ADPageControl!
    var isFromSideMenu = false

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Thoughts"
        let recent = self.storyboard?.instantiateViewControllerWithIdentifier("RecentThoughtsVC") as! RecentThoughtsTableViewController
        recent.parentVC = self
        let previous = self.storyboard?.instantiateViewControllerWithIdentifier("PreviousThoughtsVC") as! PreviousThoughtsTableViewController
        previous.parentVC = self
        arrayVC = [recent,previous]
        setupPageControl()
        if isFromSideMenu == true {
            let leftButton = UIBarButtonItem(image: UIImage(named: "MenuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(toggleSideMenu(_:)))
            self.navigationItem.leftBarButtonItem = leftButton
        }

    }

    func toggleSideMenu(sender: AnyObject)
    {
        sideMenuController.setDrawerState(.Opened, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupPageControl (){
        //page 0
        let pageModel0 = ADPageModel()
        pageModel0.strPageTitle = "RECENT THOUGHTS"
        pageModel0.iPageNumber = 0
        pageModel0.viewController = arrayVC[0] as! CCViewController
        
        let pageModel1 = ADPageModel()
        pageModel1.strPageTitle = "PREVIOUS"
        pageModel1.iPageNumber = 1
        pageModel1.bShouldLazyLoad = true
        
        pageControl = ADPageControl()
        pageControl.delegateADPageControl = self
        pageControl.arrPageModel = NSMutableArray(objects: pageModel0,pageModel1)
        
        /**** 3. Customize parameters (Optinal, as all have default value set) ****/
        
        //pageControl.iFirstVisiblePageNumber = 2;
        pageControl.iTitleViewHeight = 50;
        pageControl.iPageIndicatorHeight = 3;
        pageControl.fontTitleTabText =  UIFont(name: "Myriad Hebrew", size: 13.0)
        
        pageControl.bEnablePagesEndBounceEffect = false;
        pageControl.bEnableTitlesEndBounceEffect = false;
        
        pageControl.colorTabText = UIColor.whiteColor()
        pageControl.colorTitleBarBackground =  UIColor.blackColor()//UIColor(red: 61.0/255.0, green: 136.0/255.0, blue: 193.0/255.0, alpha: 1.0)
        pageControl.colorPageIndicator = UIColor.lightGrayColor()
        pageControl.colorPageOverscrollBackground = UIColor.whiteColor()
        
        pageControl.bShowMoreTabAvailableIndicator = false;
        
        /**** 3. Add as subview ****/
        
        pageControl.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
        self.view.addSubview(pageControl.view)
        
    }
    


}

//MARK:- PAGE CONTROL DELEGATE
extension ThoughtsViewController : ADPageControlDelegate{
    func adPageControlGetViewControllerForPageModel(pageModel: ADPageModel!) -> UIViewController! {
        return arrayVC[Int(pageModel.iPageNumber)] as! UIViewController
    }
    func adPageControlCurrentVisiblePageIndex(iCurrentVisiblePage: Int32) {
        //currentIndex = iCurrentVisiblePage
    }
}
