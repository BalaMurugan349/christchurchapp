//
//  PreviousThoughtsTableViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/9/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class PreviousThoughtsTableViewController: CCTableViewController {

    var parentVC : CCViewController!
    var nextPage = 1
    var isLoadingMoreData = true
    var isFromSideMenu = false
    var thoughts: [Thoughts] = [Thoughts]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllThoughts()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func getAllThoughts() {
        parentVC.activityIndicator.startAnimating()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            
            Thoughts.fetchAllThoughts(self.nextPage, success: { (result, nextPage) in
                self.nextPage = nextPage
                self.isLoadingMoreData = false
                self.thoughts.appendContentsOf(result as! [Thoughts])
                dispatch_async(dispatch_get_main_queue(), {
                    self.tableView.reloadData()
                    self.parentVC.activityIndicator.stopAnimating()
                    
                })
            }) { (error) in
                dispatch_async(dispatch_get_main_queue(), {
                    self.parentVC.activityIndicator.stopAnimating()
                })
                if error != nil {
                    self.isLoadingMoreData = false
                    
                }
            }
        })
        
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
        {
            if (!self.isLoadingMoreData)
            {
                self.isLoadingMoreData = true
                self.getAllThoughts()
            }
        }
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return thoughts.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let tht : Thoughts = thoughts[indexPath.row]
        return tht.thoughtTitle.heightForLabel(UIFont.systemFontOfSize(17), width: screenSize.width - 20).height +  60
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! ThoughtsTableViewCell
        let tht = thoughts[indexPath.row]
        cell.labelTitle.text = tht.thoughtTitle
        cell.labelDate.text = tht.thoughtDate.getDateForMilliseconds("MMM dd,yyyy @ hh:mm a")
            return cell
    }
    

   

}
