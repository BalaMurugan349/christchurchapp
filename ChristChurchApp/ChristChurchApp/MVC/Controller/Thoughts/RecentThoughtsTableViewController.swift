//
//  RecentThoughtsTableViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/9/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class RecentThoughtsTableViewController: CCViewController {
    
    var thought: [Thoughts] = [Thoughts]()
    var parentVC : CCViewController!
    var pageControl : BMPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getRecentThoughts()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getRecentThoughts()  {
        parentVC.activityIndicator.startAnimating()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            
            Thoughts.fetchrecentThoughts({ (result) in
                self.thought.appendContentsOf(result as! [Thoughts])
                dispatch_async(dispatch_get_main_queue(), {
                    // self.collectionView?.reloadData()
                    self.setData()
                    self.parentVC.activityIndicator.stopAnimating()
                    
                })
                
                }, failure: { (error) in
                    dispatch_async(dispatch_get_main_queue(), {
                        self.parentVC.activityIndicator.stopAnimating()
                    })
                    
            })
        })
    }
    
    func setData() {

        pageControl = BMPageControl()
        pageControl.TotalPages = thought.count
        pageControl.selectedIndex = 0
        self.view.addSubview(pageControl)
        pageControl.center = CGPointMake(self.view.center.x, screenSize.height - 290)

        let scroll : UIScrollView = UIScrollView(frame:CGRectMake(0, 0, screenSize.width, screenSize.height - 254))
        scroll.showsVerticalScrollIndicator = false
        scroll.showsHorizontalScrollIndicator = false
        scroll.backgroundColor = UIColor(red: 232.0/255.0, green: 226.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        scroll.pagingEnabled = true
        scroll.delegate = self
        self.view.addSubview(scroll)
        scroll.contentSize = CGSizeMake(CGFloat(thought.count) * screenSize.width, 0)

        for (idx,tht) in thought.enumerate(){
            let viewBG : UIView = UIView(frame: CGRectMake(screenSize.width * CGFloat(idx), 0, screenSize.width, screenSize.width - 70))
            viewBG.backgroundColor = UIColor.clearColor()
            scroll.addSubview(viewBG)
            
            let label : UILabel = UILabel(frame: CGRectMake(10, 20, screenSize.width - 20, tht.thoughtTitle.heightForLabel(UIFont.systemFontOfSize(22), width: screenSize.width - 20).height))
            label.text = tht.thoughtTitle
            label.numberOfLines = 0
            label.lineBreakMode = .ByWordWrapping
            label.font = UIFont.systemFontOfSize(22)
            label.adjustsFontSizeToFitWidth = true
            label.textColor = UIColor(red: 42.0/255.0, green: 73.0/255.0, blue: 117.0/255.0, alpha: 1.0)
            viewBG.addSubview(label)
            
            
            let labelDate : UILabel = UILabel(frame: CGRectMake(10, CGRectGetMaxY(label.frame) + 20, screenSize.width - 20, 30))
            labelDate.text = tht.thoughtDate.getDateForMilliseconds("MMM dd,yyyy @ hh:mm a")
            labelDate.font = UIFont.systemFontOfSize(14)
            labelDate.adjustsFontSizeToFitWidth = true
            labelDate.textColor = UIColor.darkGrayColor()//UIColor(red: 42.0/255.0, green: 73.0/255.0, blue: 117.0/255.0, alpha: 1.0)
            viewBG.addSubview(labelDate)
        }
        
        let imgvw : UIImageView = UIImageView(frame: CGRectMake(0, CGRectGetMaxY(scroll.frame) + 30, 100, 80))
        imgvw.image = UIImage(named: "Quote")
        imgvw.center = CGPointMake(self.view.center.x, imgvw.center.y)
        self.view.addSubview(imgvw)
        self.view.bringSubviewToFront(pageControl)

        
    }
    
    
}

//MARK:- SCROLL VIEW DELEGATE
extension RecentThoughtsTableViewController : UIScrollViewDelegate{
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let width = scrollView.frame.size.width
        let page = (scrollView.contentOffset.x + (0.5 * width))/width
        pageControl.selectedIndex = Int(page)
        
    }
    

}