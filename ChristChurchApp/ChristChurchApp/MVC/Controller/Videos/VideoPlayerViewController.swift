//
//  VideoPlayerViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/12/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit
import AVFoundation
import XCDYouTubeKit
import MediaPlayer

var videosInFullScreen : Bool = false

class VideoPlayerViewController: CCViewController {

    var videoPlayerViewController : XCDYouTubeVideoPlayerViewController!

    var video : Videos!
    @IBOutlet weak var videoContainerView : UIView!
    @IBOutlet weak var labelTitle : UILabel!
    @IBOutlet weak var labelCategory : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    var isLiveTV : Bool = false
    var isFromSideMenu : Bool = false
    var isBackButtonPressed : Bool = false
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.startAnimating()
        self.videoContainerView.subviews.forEach { subview in
            subview.removeFromSuperview()
        }
        let videoIdentifier = isLiveTV == true ? "_3TSJ0eVgc8" : getYoutubeVideoId(video.videoUrl)
        
        self.videoPlayerViewController = XCDYouTubeVideoPlayerViewController(videoIdentifier: videoIdentifier)
        
        self.videoPlayerViewController.moviePlayer.backgroundPlaybackEnabled = false
        self.videoPlayerViewController.presentInView(self.videoContainerView)
        self.videoPlayerViewController.moviePlayer.prepareToPlay()
        self.videoPlayerViewController.moviePlayer.shouldAutoplay = true
        self.videoPlayerViewController.moviePlayer.play()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(VideoPlayerViewController.moviePlayerPlaybackStateDidChange(_:)), name: MPMoviePlayerPlaybackStateDidChangeNotification, object: nil)
        
        if isLiveTV == false{
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "MMM dd,yyyy"
        let date : NSDate = dateformatter.dateFromString(video.videoDate.getDateForMilliseconds("MMM dd,yyyy"))!
        labelDate.text = NSDate().offsetFrom(date)
        labelTitle.text = video.videoTitle
        labelCategory.text = video.videoCategoryName
        }else{
            labelTitle.textAlignment = NSTextAlignment.Center
            labelTitle.text = "Christ Center Live Tv"
            self.navigationItem.title = "Live Tv"
        }
        
        if isFromSideMenu == true {
            let leftButton = UIBarButtonItem(image: UIImage(named: "MenuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(toggleSideMenu(_:)))
            self.navigationItem.leftBarButtonItem = leftButton
        }else{
            
            let leftButton : UIBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(VideoPlayerViewController.onBackButtonPressed))
            self.navigationItem.leftBarButtonItem = leftButton
        }

    }
    
    func onBackButtonPressed()  {
        videosInFullScreen = false
        isBackButtonPressed = true
        self.videoPlayerViewController.moviePlayer.stop()
        self.navigationController?.popViewControllerAnimated(true)
    }
    

    func toggleSideMenu(sender: AnyObject)
    {
        sideMenuController.setDrawerState(.Opened, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        videosInFullScreen = !isBackButtonPressed
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        videosInFullScreen = false
    }

    func getYoutubeVideoId(youtubeLink:String) -> String?{
        var youtubeId:String? = nil
        let pattern: String = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: .CaseInsensitive)
            if let regexMatch = regex.firstMatchInString(youtubeLink, options: NSMatchingOptions(rawValue: 0), range: NSRange(location: 0, length: youtubeLink.characters.count)) {
                youtubeId = (youtubeLink as NSString).substringWithRange(regexMatch.range)
            }
        }
        catch let error as NSError{
            print("Error while extracting youtube id \(error.debugDescription)")
        }
        
        return youtubeId
    }

    func moviePlayerPlaybackStateDidChange(notification: NSNotification) {
        //let moviePlayerController = notification.object as! MPMoviePlayerController
        self.activityIndicator.stopAnimating()
    }
    
}


