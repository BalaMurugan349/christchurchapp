//
//  VideoCategoryViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/11/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class VideoCategoryViewController: CCTableViewController {

    var parentVC : CCViewController!

//    var nextPage = 1
//    var isLoadingMoreData = true
    var isFromSideMenu = false
    var arrayCategories : [Categories] = [Categories]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllCategories()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getAllCategories() {
        parentVC.activityIndicator.startAnimating()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            Videos.fetchAllCategories({ (result) in
                self.arrayCategories.appendContentsOf(result as! [Categories])
                dispatch_async(dispatch_get_main_queue(), {
                    self.tableView.reloadData()
                    self.parentVC.activityIndicator.stopAnimating()
                })

                }, failure: { (error) in
                    dispatch_async(dispatch_get_main_queue(), {
                        self.parentVC.activityIndicator.stopAnimating()
                    })

            })
            
        })
        
            
            
            
//            Videos.fetchAllCategories(self.nextPage, success: { (result, nextPage) in
//                self.nextPage = nextPage
//                self.isLoadingMoreData = false
//                self.arrayCategories.appendContentsOf(result as! [Categories])
//                dispatch_async(dispatch_get_main_queue(), {
//                    self.tableView.reloadData()
//                    self.parentVC.activityIndicator.stopAnimating()
//                    
//                })
//            }) { (error) in
//                dispatch_async(dispatch_get_main_queue(), {
//                    self.parentVC.activityIndicator.stopAnimating()
//                })
//                if error != nil {
//                    self.isLoadingMoreData = false
//                    
//                }
//            }
//        })
        
    }
    
//    override func scrollViewDidScroll(scrollView: UIScrollView) {
//        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
//        {
//            if (!self.isLoadingMoreData)
//            {
//                self.isLoadingMoreData = true
//                self.getAllCategories()
//            }
//        }
//    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCategories.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return  IS_IPHONE6PLUS ? 220 : 170
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! VideosTableViewCell
        let video = arrayCategories[indexPath.row]
        cell.labelTitle.text = video.categoryDescription
        cell.labelCategory.text = video.categoryName
        cell.imageViewThumbnail.image(video.categoryImage)
        cell.imageViewThumbnail.clipsToBounds = true

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let video = arrayCategories[indexPath.row]

        let recent = self.storyboard?.instantiateViewControllerWithIdentifier("RecentVideosVC") as! RecentVideosViewController
        recent.parentVC = parentVC
        recent.categoryId = video.categoryId
        parentVC.navigationController?.pushViewController(recent, animated: true)

    }
    
    

}
