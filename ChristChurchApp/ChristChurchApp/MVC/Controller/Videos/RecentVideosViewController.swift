//
//  RecentVideosViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/11/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class RecentVideosViewController: CCTableViewController {

    var parentVC : CCViewController!
    var arrayVideos: [Videos] = [Videos]()
    var categoryId : String = "0"
    var nextPage = 1
    var isLoadingMoreData = true

    override func viewDidLoad() {
        super.viewDidLoad()

        categoryId == "0" ? getRecentVideos() : getCategoryVideos()
        self.navigationItem.title = "Videos"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getCategoryVideos()  {
        parentVC.activityIndicator.startAnimating()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            Videos.fetchCategoryVideos(self.nextPage, categoryId: self.categoryId, success: { (result, nextPage) in
                self.nextPage = nextPage
                self.isLoadingMoreData = false
                self.arrayVideos.appendContentsOf(result as! [Videos])
                dispatch_async(dispatch_get_main_queue(), {
                    self.tableView.reloadData()
                    self.parentVC.activityIndicator.stopAnimating()
                    
                })

                }, failure: { (error) in
                    dispatch_async(dispatch_get_main_queue(), {
                        self.parentVC.activityIndicator.stopAnimating()
                    })
                    if error != nil {
                        self.isLoadingMoreData = false
                        
                    }
            })
        })
    }
    
    
    
    func getRecentVideos()  {
        parentVC.activityIndicator.startAnimating()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            
            Videos.fetchrecentVideos({ (result) in
                self.arrayVideos.appendContentsOf(result as! [Videos])
                dispatch_async(dispatch_get_main_queue(), {
                     self.tableView.reloadData()
                    self.parentVC.activityIndicator.stopAnimating()
                    
                })
                
                }, failure: { (error) in
                    dispatch_async(dispatch_get_main_queue(), {
                        self.parentVC.activityIndicator.stopAnimating()
                    })
                    
            })
        })
    }

    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
        {
            if (!self.isLoadingMoreData && self.categoryId != "0")
            {
                self.isLoadingMoreData = true
                self.getCategoryVideos()
            }
        }
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayVideos.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! VideosTableViewCell
        let video = arrayVideos[indexPath.row]
        cell.labelTitle.text = video.videoTitle
        cell.labelDate.text = video.videoDate.getDateForMilliseconds("MMM dd,yyyy")
        cell.labelCategory.text = video.videoCategoryName
        cell.imageViewThumbnail.image(video.videoThumbnailUrl)
        cell.imageViewThumbnail.clipsToBounds = true
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let video : VideoPlayerViewController = self.storyboard?.instantiateViewControllerWithIdentifier("VideoPlayerVC") as! VideoPlayerViewController
        video.video = arrayVideos[indexPath.row]
        parentVC.navigationController?.pushViewController(video, animated: true)
    }


}
