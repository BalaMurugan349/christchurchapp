//
//  PrayerContainerViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/13/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit

class PrayerContainerViewController: CCViewController {
    var isFromSideMenu = false
    var arrayVC : [AnyObject]!
    var pageControl : ADPageControl!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Prayer Request"
        if isFromSideMenu == true {
            let leftButton = UIBarButtonItem(image: UIImage(named: "MenuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(toggleSideMenu(_:)))
            self.navigationItem.leftBarButtonItem = leftButton
        }
        let voice = self.storyboard?.instantiateViewControllerWithIdentifier("VoiceVC") as! VoiceViewController
        voice.parentVC = self
        let email = self.storyboard?.instantiateViewControllerWithIdentifier("EmailVC") as! EmailViewController
        email.parentVC = self
        arrayVC = [voice,email]
        setupPageControl()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func toggleSideMenu(sender: AnyObject)
    {
        sideMenuController.setDrawerState(.Opened, animated: true)
    }

    func setupPageControl (){
        //page 0
        let pageModel0 = ADPageModel()
        pageModel0.strPageTitle = "VOICE"
        pageModel0.iPageNumber = 0
        pageModel0.viewController = arrayVC[0] as! CCViewController
        
        let pageModel1 = ADPageModel()
        pageModel1.strPageTitle = "EMAIL"
        pageModel1.iPageNumber = 1
        pageModel1.bShouldLazyLoad = true
        
        pageControl = ADPageControl()
        pageControl.delegateADPageControl = self
        pageControl.arrPageModel = NSMutableArray(objects: pageModel0,pageModel1)
        
        /**** 3. Customize parameters (Optinal, as all have default value set) ****/
        
        //pageControl.iFirstVisiblePageNumber = 2;
        pageControl.iTitleViewHeight = 50;
        pageControl.iPageIndicatorHeight = 3;
        pageControl.fontTitleTabText =  UIFont(name: "Myriad Hebrew", size: 13.0)
        
        pageControl.bEnablePagesEndBounceEffect = false;
        pageControl.bEnableTitlesEndBounceEffect = false;
        
        pageControl.colorTabText = UIColor.whiteColor()
        pageControl.colorTitleBarBackground =  UIColor.blackColor()//UIColor(red: 61.0/255.0, green: 136.0/255.0, blue: 193.0/255.0, alpha: 1.0)
        pageControl.colorPageIndicator = UIColor.lightGrayColor()
        pageControl.colorPageOverscrollBackground = UIColor.whiteColor()
        
        pageControl.bShowMoreTabAvailableIndicator = false;
        
        /**** 3. Add as subview ****/
        
        pageControl.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
        self.view.addSubview(pageControl.view)
        
    }
    
}


//MARK:- PAGE CONTROL DELEGATE
extension PrayerContainerViewController : ADPageControlDelegate{
    func adPageControlGetViewControllerForPageModel(pageModel: ADPageModel!) -> UIViewController! {
        return arrayVC[Int(pageModel.iPageNumber)] as! UIViewController
    }
    func adPageControlCurrentVisiblePageIndex(iCurrentVisiblePage: Int32) {
        //currentIndex = iCurrentVisiblePage
    }
}
