//
//  VoiceViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/14/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit
import MessageUI


class VoiceViewController: CCViewController {
    var moviePlayer : MPMoviePlayerController!
    var parentVC : CCViewController!
    var btnRecord : UIButton!
    var arrayAudio : [Audio] = [Audio]()
    @IBOutlet weak var tableviewAudio : UITableView!
    var toEmail : String = "srishtimedia@gmail.com"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        arrayAudio = Audio.fetchAudioFiles()
        tableviewAudio.reloadData()
        
    }
    
    func fetchToEmail()  {
        parentVC.activityIndicator.startAnimating()
        CCGeneral.fetchLiveTvUrl({ (result) in
            self.toEmail = result["prayer_voice_email"] as! String
            self.parentVC.activityIndicator.stopAnimating()
            
        }) { (error) in
            self.parentVC.activityIndicator.stopAnimating()
            
        }
        
    }
    
    @IBAction func recordButtonAction()  {
        let record : RecordViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RecordVC") as! RecordViewController
        parentVC.navigationController?.pushViewController(record, animated: true)
        
    }
    
    //MARK:- TABLEVIEW DELEGATES
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayAudio.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let audioObj : Audio = arrayAudio[indexPath.row]
        return audioObj.isExpanded == 0 ? 70 : 105
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! AudioTableViewCell
        cell.configureCell(arrayAudio[indexPath.row])
        cell.buttonDelete.addTarget(self, action: #selector(VoiceViewController.deleteCell(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cell.buttonDelete.tag = indexPath.row
        cell.buttonMail.addTarget(self, action: #selector(VoiceViewController.mailAudio(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cell.buttonPlay.addTarget(self, action: #selector(VoiceViewController.playButtonPressed(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        cell.buttonMail.tag = indexPath.row
        cell.buttonPlay.tag = indexPath.row
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        for (idx,audioObj) in arrayAudio.enumerate(){
            audioObj.isExpanded = idx == indexPath.row ? NSNumber(bool: !audioObj.isExpanded!.boolValue) : false
            
        }
        tableviewAudio.reloadData()
    }
    
    //MARK:- TABLEVIEWCELL BUTTON ACTIONS
    func playButtonPressed(sender : UIButton)  {
        playAudio(arrayAudio[sender.tag])
        
    }
    
    func deleteCell(sender : UIButton)  {
        let alertController = UIAlertController(title: "Christ Center", message: "Are you sure you want to delete this audio", preferredStyle: UIAlertControllerStyle.Alert)
        let alertNoAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) { (action) -> Void in
        }
        let alertYesAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.Default) { (action) -> Void in
            Audio.deleteAudio(self.arrayAudio[sender.tag])
            self.arrayAudio = Audio.fetchAudioFiles()
            self.tableviewAudio.reloadData()
            
        }
        alertController.addAction(alertNoAction)
        alertController.addAction(alertYesAction)
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    func mailAudio(sender : UIButton)  {
        let mailComposeViewController = configuredMailComposeViewController(sender.tag)
        if MFMailComposeViewController.canSendMail() {
            parentVC.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    //MARK:- MAIL COMPOSER
    func configuredMailComposeViewController(index : Int) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        let audioObj = arrayAudio[index]
        mailComposerVC.setToRecipients([toEmail])
        mailComposerVC.setSubject("Prayer Request - \(getMailDate())")
        mailComposerVC.setMessageBody("Voice prayer request from Christ church app", isHTML: false)
        self.activityIndicator.startAnimating()
        if let fileData = NSData(contentsOfFile: getDocumentsDirectory() + "/\(audioObj.name!)") {
            mailComposerVC.addAttachmentData(fileData, mimeType: "audio/wav", fileName: audioObj.name!)
            self.activityIndicator.stopAnimating()
        }
        return mailComposerVC
    }
    
    func getMailDate() -> String {
        let dateFormatter : NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.stringFromDate(NSDate())
        
    }
    
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    func playAudio(audioObj : Audio)  {
        let filePath = getDocumentsDirectory() + "/\(audioObj.name!)"
        moviePlayer = MPMoviePlayerController(contentURL: NSURL(fileURLWithPath: filePath))
        moviePlayer.scalingMode = MPMovieScalingMode.AspectFill
        moviePlayer.view.frame = self.view.bounds
        moviePlayer.movieSourceType = MPMovieSourceType.File
        moviePlayer.prepareToPlay()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(VoiceViewController.doneMoviePlayerButtonClicked(_:)), name: MPMoviePlayerWillExitFullscreenNotification, object: moviePlayer)
        moviePlayer.controlStyle = MPMovieControlStyle.Embedded
        moviePlayer.play()
        self.view.addSubview(moviePlayer.view)
        moviePlayer.fullscreen = true
        
    }
    
    func getDocumentsDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func doneMoviePlayerButtonClicked(aNotification : NSNotification)  {
        let player : MPMoviePlayerController = aNotification.object as! MPMoviePlayerController
        player.stop()
        player.view.removeFromSuperview()
    }
    
    
}

extension VoiceViewController : MFMailComposeViewControllerDelegate{
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
