//
//  EmailViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/14/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit
import MessageUI

class EmailViewController: CCTableViewController {
    
    var parentVC : CCViewController!
    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldEmail : UITextField!
    @IBOutlet weak var textviewPrayerRequest : UITextView!
    var toEmail : String = "srishtimedia.in@gmail.com"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchToEmail()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSendButtonPressed (Sender : UIButton){
        if textfieldName.isEmpty || textfieldEmail.isEmpty || textviewPrayerRequest.text == "Prayer Request"
        {
            let alert = Extention.alert("Please enter all the fields")
            parentVC.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldEmail.text!.isValidEmail{
            let alert = Extention.alert("Please enter the valid email")
            parentVC.presentViewController(alert, animated: true, completion: nil)
        }else{
            let mailComposeViewController = configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                parentVC.presentViewController(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }
            
        }
    }
    
    func fetchToEmail()  {
        parentVC.activityIndicator.startAnimating()
        CCGeneral.fetchLiveTvUrl({ (result) in
            self.toEmail = result["prayer_form_email"] as! String
            self.parentVC.activityIndicator.stopAnimating()
            
        }) { (error) in
            self.parentVC.activityIndicator.stopAnimating()
            
        }
        
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([toEmail])
        mailComposerVC.setSubject("Prayer Request by \(textfieldName.text!)")
        mailComposerVC.setMessageBody("Name : \(textfieldName.text!) \n Email : \(textfieldEmail.text!) \n Request : \(textviewPrayerRequest.text!)", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
}

extension EmailViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension EmailViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "Prayer Request" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Prayer Request"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension EmailViewController : MFMailComposeViewControllerDelegate{
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
