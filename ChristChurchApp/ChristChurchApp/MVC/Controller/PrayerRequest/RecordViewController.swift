//
//  RecordViewController.swift
//  ChristChurchApp
//
//  Created by Bala Murugan on 8/14/16.
//  Copyright © 2016 Bose. All rights reserved.
//

import UIKit
import AVFoundation

class RecordViewController: CCViewController {
    @IBOutlet weak var btnRecord : UIButton!
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var moviePlayer : MPMoviePlayerController!
    var isRedordingFinished : Bool = false
    var timer : NSTimer!
    @IBOutlet weak var labelDuration : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Record Voice"
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] (allowed: Bool) -> Void in
                dispatch_async(dispatch_get_main_queue()) {
                    if allowed {
                        // self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
        
        
        self.navigationItem.hidesBackButton = true
        let backButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Back"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(RecordViewController.onBackButtonPressed))
        self.navigationItem.leftBarButtonItem = backButton
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func onBackButtonPressed() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- DONE NAVIGATION BUTTON ACTION
    func onDoneNavigationButtonPressed()  {
        if isRedordingFinished == true{
            let alertController = UIAlertController(title: "", message: "Please enter the audio name", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addTextFieldWithConfigurationHandler { (txtfld) in
                txtfld.keyboardType = UIKeyboardType.Default
            }
            let alertNoAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) { (action) -> Void in
            }
            let alertYesAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.Default) { (action) -> Void in
                let txtfld : UITextField = alertController.textFields!.first!
                if txtfld.text != "" {
                    self.saveAudio(txtfld.text!)
                }
                
            }
            alertController.addAction(alertNoAction)
            alertController.addAction(alertYesAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            
            
        }else{
            let alert = Extention.alert("Please record the audio")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
        
    }
    
    //MARK:- SAVE AUDIO
    func saveAudio(filename : String)  {
        do {
            let documentDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0])
            let originPath = documentDirectory.URLByAppendingPathComponent("/recording.m4a")
            let destinationPath = documentDirectory.URLByAppendingPathComponent("/\(filename).m4a")
            let fileExists = NSFileManager.defaultManager().fileExistsAtPath(destinationPath!.path!)
            if fileExists{
                let alert = Extention.alert("Filename already exists")
                self.presentViewController(alert, animated: true, completion: nil)
                
            }else{
                isRedordingFinished = false
                btnRecord.selected = false
                btnRecord.setImage(UIImage(named: "Record1"), forState: UIControlState.Normal)
                
                getAudioDuration()
                calculateFileSize()
                Audio.saveAudioToCoreData("\(filename).m4a", createdDate: getCreatedDate(), duration: getAudioDuration(), fileSize: calculateFileSize())
                try NSFileManager.defaultManager().moveItemAtURL(originPath!, toURL: destinationPath!)
                self.navigationController?.popViewControllerAnimated(true)
            }
        } catch let error as NSError {
            print(error)
        }
        
        
    }
    
    func getCreatedDate() -> String {
        let dateFormatter : NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, hh:mm a"
        return dateFormatter.stringFromDate(NSDate())
        
    }
    
    //MARK:- GET AUDIO DURATION
    func getAudioDuration() -> String {
        do {
            let sound : AVAudioPlayer = try AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: getDocumentsDirectory() + "/recording.m4a"))
            return self.stringFromTimeInterval(sound.duration) as String
        } catch {
            
        }
        return ""
    }
    func stringFromTimeInterval(interval:NSTimeInterval) -> NSString {
        let ti = NSInteger(interval)
        // let ms = Int((interval % 1) * 1000)
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        // let hours = (ti / 3600)
        let duration = NSString(format: "%0.2d:%0.2d",minutes,seconds)
        
        return duration
    }
    
    //MARK:- CALCULATE FILE SIZE
    func calculateFileSize() -> String {
        let filePath = getDocumentsDirectory() + "/recording.m4a"
        var fileSize : UInt64 = 0
        
        do {
            let attr : NSDictionary? = try NSFileManager.defaultManager().attributesOfItemAtPath(filePath)
            
            if let _attr = attr {
                fileSize = _attr.fileSize();
                let sizeKB = Double(fileSize / 1024)
                let sizeMB = Double(sizeKB / 1024)
                var finalSize = ""
                if Int(sizeMB) > 0{
                    finalSize = String(format: "%.2f", sizeMB) + " MB"
                }else{
                    finalSize = String(format: "%.2f", sizeKB) + " KB"
                }
                
                return finalSize
            }
        } catch {
            print("Error: \(error)")
        }
        return ""
        
    }
    
    //MARK:- START RECORDING
    @IBAction func startRecordingAudio (sender : UIButton){
        if sender.selected == false{
            recordAudio()
        }else{
            // playAudio()
            finishRecording(success: true)
        }
        
    }
    
    func recordAudio()  {
        btnRecord.setImage(UIImage(named: "Recording"), forState: UIControlState.Normal)
        let audioFilename = getDocumentsDirectory() + "/recording.m4a"
        let audioURL = NSURL(fileURLWithPath: audioFilename)
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000.0,
            AVNumberOfChannelsKey: 1 as NSNumber,
            AVEncoderAudioQualityKey: AVAudioQuality.High.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(URL: audioURL, settings: settings)
            audioRecorder.delegate = self
            timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(RecordViewController.updateDuration), userInfo: nil, repeats: true)
            
            audioRecorder.record()
        } catch {
            finishRecording(success: false)
        }
    }
    func getDocumentsDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func finishRecording(success success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        btnRecord.selected = true
        isRedordingFinished = true
        timer.invalidate()
        labelDuration.text = "00:00"
        btnRecord.setImage(UIImage(named: "Record1"), forState: UIControlState.Normal)
    }
    
    //MARK:- STOP RECORDING
    @IBAction func endRecordingAudio (Sender : UIButton){
        if btnRecord.selected == false{
            startRecordingAudio(Sender)
        }else{
            finishRecording(success: true)
            onDoneNavigationButtonPressed()
            
        }
        Sender.selected = !Sender.selected
        
    }
    
    //MARK:- PLAY AUDIO
    func playAudio()  {
        let filePath = getDocumentsDirectory() + "/recording.m4a"
        moviePlayer = MPMoviePlayerController(contentURL: NSURL(fileURLWithPath: filePath))
        moviePlayer.scalingMode = MPMovieScalingMode.AspectFill
        moviePlayer.view.frame = self.view.bounds
        moviePlayer.movieSourceType = MPMovieSourceType.File
        moviePlayer.prepareToPlay()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RecordViewController.doneMoviePlayerButtonClicked(_:)), name: MPMoviePlayerWillExitFullscreenNotification, object: moviePlayer)
        moviePlayer.controlStyle = MPMovieControlStyle.Embedded
        moviePlayer.play()
        self.view.addSubview(moviePlayer.view)
        moviePlayer.fullscreen = true
        
    }
    
    func doneMoviePlayerButtonClicked(aNotification : NSNotification)  {
        let player : MPMoviePlayerController = aNotification.object as! MPMoviePlayerController
        player.stop()
        player.view.removeFromSuperview()
    }
    
    
    //MARK:- UPDATE AUDIO DURATION
    func updateDuration()  {
        let minutes = floor(audioRecorder.currentTime/60);
        let seconds = audioRecorder.currentTime - (minutes * 60);
        
        labelDuration.text = "\(String(format: "%02d", Int(minutes))):\(String(format: "%02d", Int(seconds) + 1))"
    }
    
}

//MARK:- AUDIORECORDER DELEGATE
extension RecordViewController : AVAudioRecorderDelegate{
    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
}
